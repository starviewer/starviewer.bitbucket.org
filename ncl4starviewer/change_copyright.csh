#!/bin/csh -f

 set echo

 set fls = `find . -type f -name Copyright -print`

#echo $fls

 foreach f ( $fls )
   #set diffs = `diff $base $f`
   #if("" != "$diffs") then
   #    echo $f
   #endif

    cp copyright.base $f
 end

 cp copyright.base Copyright
 cat copyright.ext >> Copyright

 set ifls = `find install -type f -name Copyright -print`
 foreach f ( $ifls )
    cat install/copyright.ext >> $f
 end

