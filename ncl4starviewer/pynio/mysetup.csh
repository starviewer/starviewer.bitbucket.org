#!/bin/csh -f
#$Id: mysetup.csh 16178 2015-04-12 16:54:47Z huangwei $

 set echo

 setenc CC		clang

 setenv HAS_ZLIB	1
 setenv HAS_SZIP	1
 setenv HAS_HDF5	1
 setenv USE_GFORTRAN	1
 setenv HAS_NETCDF4	1
 setenv HAS_HDF4	0
 setenv HAS_HDFEOS	0
 setenv HAS_HDFEOS5	0
 setenv HAS_GRIB2	1
 setenv HAS_GDAL	1

 setenv CFLAGS		"-O0 -g"

 setenv PREFIX		/usr/local

 setenv NETCDF_PREFIX	/opt/local
 setenv ZLIB_PREFIX	/opt/local
 setenv HDF5_PREFIX	/usr/local

 setenv ZLIB_INCDIR	/opt/local/include
 setenv ZLIB_LIBDIR	/opt/local/lib

 setenv HDF5_INCDIR	/usr/local/include
 setenv HDF5_LIBDIR	/usr/local/lib
 setenv NETCDF_INCDIR	/usr/local/include
 setenv NETCDF_LIBDIR	/usr/local/lib

 setenv F2CLIBS_PREFIX	/opt/local/lib/gcc6
 setenv F2CLIBS	gfortran

 python setup.py install --prefix=${PREFIX}

