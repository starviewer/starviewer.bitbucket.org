import numpy 
import Nio 
import time, os

opt = Nio.options()
opt.Format = 'NetCDF4'

print opt.Format

#create a file
hatt = "Created at " + time.ctime(time.time())
fn = "pynio_created.nc"
if(os.path.isfile(fn)):
    os.remove(fn)
file = Nio.open_file(fn, options=opt, history=hatt, mode="w")

#create global attributes
file.source   = "Nio created NetCDF4 file"
#setattr(file, 'source', "Nio test NetCDF file")
file.history = "Created " + time.ctime(time.time())

ntimes = 5
nlevels = 10
nlats = 73
nlons = 144

#Create some dimensions.
file.create_dimension("time", None)
file.create_dimension("level", None)
file.create_dimension("lat", nlats)
file.create_dimension("lon", nlons)

print "file:\n"
print file

mtimes = 1
mlevels = (nlevels+1)/2
mlats = (nlats+1)/2
mlons = (nlons+1)/2

print "mtimes: %d, mlevels: %d, mlats: %d, mlons: %d" %(mtimes, mlevels, mlats, mlons)

#Create chunk dimensions.
file.create_chunk_dimension("time", mtimes)
file.create_chunk_dimension("level", mlevels)
file.create_chunk_dimension("lat", mlats)
file.create_chunk_dimension("lon", mlons)

print "part 5"

#Create some variables.
#import pdb; pdb.set_trace()
time  = file.create_variable("time",  "d", ("time",))
level = file.create_variable("level", "i", ("level",))
lat   = file.create_variable("lat",   "f", ("lat",))
lon   = file.create_variable("lon",   "f", ("lon",))
temp  = file.create_variable("temp" , "f", ("time", "level", "lat", "lon"))
#temp  = file.create_variable("temp" , "d", ("time", "level", "lat", "lon"))

print "part 6"

#Specify attributes.
time.units = "hours since 0001-01-01 00:00:00.0"
time.calendar = "gregorian"
level.units = "hPa"
lat.units = "degrees north"
lon.units = "degrees east"
temp.units = "K"

#fill in variables.
time[:] = [0.0, 12.0, 24.0, 36.0, 48.0]
level[:] =  [1000, 850, 700, 500, 300, 250, 200, 150, 100, 50]
latvalues = numpy.arange(-90, 91, 2.5, 'float32')
lonvalues = numpy.arange(-180, 180, 2.5, 'float32')
print('lat =\n',latvalues[:])
#print('lon =\n',lonvalues[:])

#print('time shape = ', time.shape)
#print('level shape = ', level.shape)
tshape = time.shape
lshape = level.shape
print('time shape = ', tshape[0])
print('level shape = ', lshape[0])

#file.set_dimension("time", tshape[0])
#file.set_dimension("level", lshape[0])

from numpy.random.mtrand import uniform # random number generator.
tempvalues = uniform(200.0, 300.0, size=(ntimes, nlevels, nlats, nlons))
#print('temp shape = ', tempvalues.shape)
#print('temp dtype = ', tempvalues.dtype)
#print tempvalues
ftempvalues = numpy.ndarray(shape=(ntimes, nlevels, nlats, nlons), dtype=numpy.float32)
#ftempvalues = numpy.array(tempvalues, dtype=numpy.float)
#ftempvalues = tempvalues.view('float32')
#ftempvalues = tempvalues

print('ftemp dtype = ', ftempvalues.dtype)

ftempvalues[:,:,:,:] = tempvalues[:,:,:,:]

print('ftemp shape = ', ftempvalues.shape)
print('ftemp dtype = ', ftempvalues.dtype)
print "max: %f, min: %f" %(numpy.amax(tempvalues), numpy.amin(tempvalues))
print "max: %f, min: %f" %(numpy.amax(ftempvalues), numpy.amin(ftempvalues))

print file.variables
print file.variables['time'][:]
print "writing lat"
lat[:] = latvalues
lon[:] = lonvalues
temp[:] = ftempvalues

