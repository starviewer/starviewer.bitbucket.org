#!/bin/csh -f

 set echo
 
 foreach f ( \
	nio01.py \
	nio02.py \
	nio03.py \
	nio04.py \
	nio05.py \
	rcomp.py \
	wcomp.py \
	rstr.py \
	wstr.py \
	rvlen.py \
	wvlen.py \
	read_nc4.py \
	write_nc4.py \
        )
    echo ""
    echo ""
    echo ""
    echo "Starting file: $f"
    python $f
    echo "Finished file: $f"
 end

