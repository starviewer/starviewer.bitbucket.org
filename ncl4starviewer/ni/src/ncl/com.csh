#!/bin/csh -f

 set echo

 g++ -ansi -fPIC -m64 -Wall -fopenmp \
	-F/Applications/Qt/Desktop/Qt/474/gcc/lib \
	-framework QtOpenGL -framework QtGui -framework QtCore -framework OpenGL -framework AGL \
	-g -o ncl \
	Ncl.o io.o NclHLUObj.o NclApi.o guiFuncs.o NclDriver.o \
	scanner.o parser.o MathFuncs.o NclTypedouble.o NclTypefloat.o \
	NclTypeint.o NclTypelogical.o \
	NclTypelong.o NclTypeobj.o \
	NclTypeshort.o NclTypestring.o \
	NclTypeint64.o NclTypeuint64.o \
	NclTypeubyte.o \
	NclTypeushort.o NclTypeuint.o NclTypeulong.o \
	NclTypebyte.o NclTypechar.o TypeSupport.o \
	NclMultiDValData.o AddFileFormats.o AttSupport.o DataSupport.o \
	FileSupport.o Formats.o GetGrids.o \
	AdvancedFileSupport.o \
	NclAdvancedFile.o NclAdvancedGroup.o \
	InitData.o Memory.o NclAtt.o NclCCM.o \
	NclCoordVar.o NclData.o NclGroup.o NclFile.o NclFileVar.o \
	NclGRIB.o NclMultiDValnclfileData.o \
	NclNetCdf.o NclOneDValCoordData.o NclType.o \
	NclTypelist.o NclVar.o VarSupport.o \
	ctoiee.o date.o qu2reg3.o rowina3.o scm0.o ncepcode.o \
	NclHDFEOS.o NclHDF.o \
	NclHDFEOS5.o NclNewHDFEOS5.o NclNewHDF5.o NclHDF5.o h5reader.o h5writer.o \
	NclNetCDF4.o \
	NclGRIB2.o NclOGR.o NclAdvancedOGR.o AddBuiltIns.o AddHLUObjs.o \
	AddIntrinsics.o Execute.o \
	Machine.o OpsFuncs.o SrcTree.o \
	Symbol.o Translate.o \
	NclHLUVar.o HLUSupport.o \
	NclMultiDValHLUObjData.o \
	BuiltInSupport.o BuiltInFuncs.o \
	javaAddProto.o javaAddFuncs.o \
	userAddProto.o userAddFuncs.o \
	HLUFunctions.o yywrap.o craybin.o \
	complete.o editline.o sysunix.o ListSupport.o \
	NclList.o NclMultiDVallistData.o \
	NclProf.o NclTime.o fortranio.o \
	-L../../.././ni/src/lib/nfp -lnfp \
	-L../../.././ni/src/lib/nfpfort -lnfpfort \
	-L../../.././ni/src/lib/hlu -lhlu \
	-L../../.././ncarg2d/src/libncarg -lncarg \
	-L../../.././ncarg2d/src/libncarg_gks -lncarg_gks \
	-L../../.././common/src/libncarg_c -lncarg_c \
	-L../../.././ngmath/src/lib -lngmath \
	-L/Users/huangwei/ncl/NCLDEV/lib \
	-L/Users/huangwei/ncl/lib/netcdf/4.3.0/lib \
	-L/Users/huangwei/ncl/lib/hdf/4.2.8/lib \
	-L/Users/huangwei/ncl/lib/hdfeos/2.18v1.00/lib \
	-L/Users/huangwei/ncl/lib/hdfeos5/1.14/lib \
	-L/Users/huangwei/ncl/lib/hdf5/1.8.11/lib \
	-L/Users/huangwei/ncl/lib/cairo/1.12.16/lib \
	-L/Users/huangwei/ncl/lib/zlib/1.2.8/lib \
	-L/Users/huangwei/ncl/lib/szip/2.1/lib \
	-L/usr/local/gcc/4.7.2/lib \
	-L/usr/local/pixman/0.26.2/lib \
	-L/usr/X11/lib \
	-L/usr/lib \
	-L/usr/local/lib \
	-L/opt/local/lib \
	-lnetcdf -lcurl \
	-lhe5_hdfeos -lGctp \
	-lhdf5_hl -lhdf5 -lsz \
	-lhdfeos -lGctp \
	-lmfhdf -ldf \
	-ljpeg -lz -lsz \
	-lhdf5_hl -lhdf5 -lsz -lz \
	-lgdal -lproj -ljpeg -lxml2 \
	-ludunits2 -lexpat \
	-lgrib2c -ljasper -lpng -lz \
	-ljpeg -lpng -lz \
	-lcairo -lfontconfig \
	-lpixman-1 -lfreetype -lexpat -lpng -lz \
	-lpthread -lbz2 \
	-lXrender \
	-L../../.././external/sphere3.1_dp -lsphere3.1_dp \
	-L../../.././external/fftpack5_dp -lfftpack5_dp \
	-L../../.././external/lapack -llapack_ncl \
	-L../../.././external/blas -lblas_ncl \
	-lXpm -lX11 -lXext -lgfortran -lm -ldl  

