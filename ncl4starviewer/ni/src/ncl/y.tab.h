#define EOLN 257
#define EOFF 258
#define RP 259
#define LP 260
#define RBC 261
#define LBC 262
#define RBK 263
#define LBK 264
#define COLON 265
#define SEMI 266
#define MARKER 267
#define LPSLSH 268
#define SLSHRP 269
#define DIM_MARKER 270
#define FSTRING 271
#define EFSTRING 272
#define ASTRING 273
#define CSTRING 274
#define GSTRING 275
#define LBKSLSH 276
#define SLSHRBK 277
#define DIMNUM 278
#define INT 279
#define REAL 280
#define DIM 281
#define DIMNAME 282
#define ATTNAME 283
#define COORDV 284
#define FVAR 285
#define GVAR 286
#define STRING 287
#define INTEGER 288
#define UINT 289
#define FLOAT 290
#define LONG 291
#define ULONG 292
#define INT64 293
#define UINT64 294
#define DOUBLE 295
#define BYTE 296
#define UBYTE 297
#define CHARACTER 298
#define GRAPHIC 299
#define STRNG 300
#define NUMERIC 301
#define ENUMERIC 302
#define SNUMERIC 303
#define FILETYPE 304
#define SHORT 305
#define USHORT 306
#define LOGICAL 307
#define GROUP 308
#define GROUPTYPE 309
#define COMPOUND 310
#define UNDEFFILEGROUP 311
#define UNDEF 312
#define VAR 313
#define WHILE 314
#define DO 315
#define QUIT 316
#define NPROC 317
#define PIPROC 318
#define IPROC 319
#define UNDEFFILEVAR 320
#define BREAK 321
#define NOPARENT 322
#define NCLNULL 323
#define LIST 324
#define BGIN 325
#define END 326
#define NFUNC 327
#define IFUNC 328
#define FDIM 329
#define IF 330
#define THEN 331
#define VBLKNAME 332
#define CONTINUE 333
#define DFILE 334
#define KEYFUNC 335
#define KEYPROC 336
#define ELSE 337
#define EXTERNAL 338
#define NCLEXTERNAL 339
#define RETURN 340
#define VSBLKGET 341
#define NEW 342
#define OBJVAR 343
#define OBJTYPE 344
#define RECORD 345
#define VSBLKCREATE 346
#define VSBLKSET 347
#define LOCAL 348
#define STOP 349
#define NCLTRUE 350
#define NCLFALSE 351
#define NCLMISSING 352
#define DLIB 353
#define REASSIGN 354
#define OR 355
#define XOR 356
#define AND 357
#define GT 358
#define GE 359
#define LT 360
#define LE 361
#define EQ 362
#define NE 363
#define UNOP 364
#define NOT 365
typedef union {
	int integer;
	long long int_val;
	double real;
	char  str[NCL_MAX_STRING];
        char *sstr;
	struct _NclSymbol *sym;
	void *src_node;
	struct src_node_list *list;
	struct ncl_rcl_list *array;
	struct ncl_rcl_list *listvar;
} YYSTYPE;
extern YYSTYPE yylval;
