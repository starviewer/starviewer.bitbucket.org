
/*
 *      $Id: MathFuncs.h.sed,v 1.1 1995-01-31 22:25:53 ethan Exp $
 */
/************************************************************************
*									*
*			     Copyright (C)  1995			*
*	     University Corporation for Atmospheric Research		*
*			     All Rights Reserved			*
*									*
************************************************************************/
/*
 *	File:		
 *
 *	Author:		Ethan Alpert
 *			National Center for Atmospheric Research
 *			PO 3000, Boulder, Colorado
 *
 *	Date:		Tue Jan 31 15:17:33 MST 1995
 *
 *	Description:	
 */
#ifndef MathFuncs_h
#define MathFuncs_h



/*
 *      $Id: MathTemplate.h.sed,v 1.1 1995-01-31 22:25:56 ethan Exp $
 */
/************************************************************************
*									*
*			     Copyright (C)  1995			*
*	     University Corporation for Atmospheric Research		*
*			     All Rights Reserved			*
*									*
************************************************************************/
/*
 *	File:		
 *
 *	Author:		Ethan Alpert
 *			National Center for Atmospheric Research
 *			PO 3000, Boulder, Colorado
 *
 *	Date:		Tue Jan 31 15:17:01 MST 1995
 *
 *	Description:	
 */
extern NhlErrorTypes _Nclsinh(
#if     NhlNeedProto
void
#endif
);


/*
 *      $Id: MathTemplate.h.sed,v 1.1 1995-01-31 22:25:56 ethan Exp $
 */
/************************************************************************
*									*
*			     Copyright (C)  1995			*
*	     University Corporation for Atmospheric Research		*
*			     All Rights Reserved			*
*									*
************************************************************************/
/*
 *	File:		
 *
 *	Author:		Ethan Alpert
 *			National Center for Atmospheric Research
 *			PO 3000, Boulder, Colorado
 *
 *	Date:		Tue Jan 31 15:17:01 MST 1995
 *
 *	Description:	
 */
extern NhlErrorTypes _Nclcosh(
#if     NhlNeedProto
void
#endif
);


/*
 *      $Id: MathTemplate.h.sed,v 1.1 1995-01-31 22:25:56 ethan Exp $
 */
/************************************************************************
*									*
*			     Copyright (C)  1995			*
*	     University Corporation for Atmospheric Research		*
*			     All Rights Reserved			*
*									*
************************************************************************/
/*
 *	File:		
 *
 *	Author:		Ethan Alpert
 *			National Center for Atmospheric Research
 *			PO 3000, Boulder, Colorado
 *
 *	Date:		Tue Jan 31 15:17:01 MST 1995
 *
 *	Description:	
 */
extern NhlErrorTypes _Ncltanh(
#if     NhlNeedProto
void
#endif
);


/*
 *      $Id: MathTemplate.h.sed,v 1.1 1995-01-31 22:25:56 ethan Exp $
 */
/************************************************************************
*									*
*			     Copyright (C)  1995			*
*	     University Corporation for Atmospheric Research		*
*			     All Rights Reserved			*
*									*
************************************************************************/
/*
 *	File:		
 *
 *	Author:		Ethan Alpert
 *			National Center for Atmospheric Research
 *			PO 3000, Boulder, Colorado
 *
 *	Date:		Tue Jan 31 15:17:01 MST 1995
 *
 *	Description:	
 */
extern NhlErrorTypes _Nclsin(
#if     NhlNeedProto
void
#endif
);


/*
 *      $Id: MathTemplate.h.sed,v 1.1 1995-01-31 22:25:56 ethan Exp $
 */
/************************************************************************
*									*
*			     Copyright (C)  1995			*
*	     University Corporation for Atmospheric Research		*
*			     All Rights Reserved			*
*									*
************************************************************************/
/*
 *	File:		
 *
 *	Author:		Ethan Alpert
 *			National Center for Atmospheric Research
 *			PO 3000, Boulder, Colorado
 *
 *	Date:		Tue Jan 31 15:17:01 MST 1995
 *
 *	Description:	
 */
extern NhlErrorTypes _Nclcos(
#if     NhlNeedProto
void
#endif
);


/*
 *      $Id: MathTemplate.h.sed,v 1.1 1995-01-31 22:25:56 ethan Exp $
 */
/************************************************************************
*									*
*			     Copyright (C)  1995			*
*	     University Corporation for Atmospheric Research		*
*			     All Rights Reserved			*
*									*
************************************************************************/
/*
 *	File:		
 *
 *	Author:		Ethan Alpert
 *			National Center for Atmospheric Research
 *			PO 3000, Boulder, Colorado
 *
 *	Date:		Tue Jan 31 15:17:01 MST 1995
 *
 *	Description:	
 */
extern NhlErrorTypes _Ncltan(
#if     NhlNeedProto
void
#endif
);


/*
 *      $Id: MathTemplate.h.sed,v 1.1 1995-01-31 22:25:56 ethan Exp $
 */
/************************************************************************
*									*
*			     Copyright (C)  1995			*
*	     University Corporation for Atmospheric Research		*
*			     All Rights Reserved			*
*									*
************************************************************************/
/*
 *	File:		
 *
 *	Author:		Ethan Alpert
 *			National Center for Atmospheric Research
 *			PO 3000, Boulder, Colorado
 *
 *	Date:		Tue Jan 31 15:17:01 MST 1995
 *
 *	Description:	
 */
extern NhlErrorTypes _Nclasin(
#if     NhlNeedProto
void
#endif
);


/*
 *      $Id: MathTemplate.h.sed,v 1.1 1995-01-31 22:25:56 ethan Exp $
 */
/************************************************************************
*									*
*			     Copyright (C)  1995			*
*	     University Corporation for Atmospheric Research		*
*			     All Rights Reserved			*
*									*
************************************************************************/
/*
 *	File:		
 *
 *	Author:		Ethan Alpert
 *			National Center for Atmospheric Research
 *			PO 3000, Boulder, Colorado
 *
 *	Date:		Tue Jan 31 15:17:01 MST 1995
 *
 *	Description:	
 */
extern NhlErrorTypes _Nclacos(
#if     NhlNeedProto
void
#endif
);


/*
 *      $Id: MathTemplate.h.sed,v 1.1 1995-01-31 22:25:56 ethan Exp $
 */
/************************************************************************
*									*
*			     Copyright (C)  1995			*
*	     University Corporation for Atmospheric Research		*
*			     All Rights Reserved			*
*									*
************************************************************************/
/*
 *	File:		
 *
 *	Author:		Ethan Alpert
 *			National Center for Atmospheric Research
 *			PO 3000, Boulder, Colorado
 *
 *	Date:		Tue Jan 31 15:17:01 MST 1995
 *
 *	Description:	
 */
extern NhlErrorTypes _Nclatan(
#if     NhlNeedProto
void
#endif
);


/*
 *      $Id: MathTemplate.h.sed,v 1.1 1995-01-31 22:25:56 ethan Exp $
 */
/************************************************************************
*									*
*			     Copyright (C)  1995			*
*	     University Corporation for Atmospheric Research		*
*			     All Rights Reserved			*
*									*
************************************************************************/
/*
 *	File:		
 *
 *	Author:		Ethan Alpert
 *			National Center for Atmospheric Research
 *			PO 3000, Boulder, Colorado
 *
 *	Date:		Tue Jan 31 15:17:01 MST 1995
 *
 *	Description:	
 */
extern NhlErrorTypes _Nclceil(
#if     NhlNeedProto
void
#endif
);


/*
 *      $Id: MathTemplate.h.sed,v 1.1 1995-01-31 22:25:56 ethan Exp $
 */
/************************************************************************
*									*
*			     Copyright (C)  1995			*
*	     University Corporation for Atmospheric Research		*
*			     All Rights Reserved			*
*									*
************************************************************************/
/*
 *	File:		
 *
 *	Author:		Ethan Alpert
 *			National Center for Atmospheric Research
 *			PO 3000, Boulder, Colorado
 *
 *	Date:		Tue Jan 31 15:17:01 MST 1995
 *
 *	Description:	
 */
extern NhlErrorTypes _Nclfloor(
#if     NhlNeedProto
void
#endif
);


/*
 *      $Id: MathTemplate.h.sed,v 1.1 1995-01-31 22:25:56 ethan Exp $
 */
/************************************************************************
*									*
*			     Copyright (C)  1995			*
*	     University Corporation for Atmospheric Research		*
*			     All Rights Reserved			*
*									*
************************************************************************/
/*
 *	File:		
 *
 *	Author:		Ethan Alpert
 *			National Center for Atmospheric Research
 *			PO 3000, Boulder, Colorado
 *
 *	Date:		Tue Jan 31 15:17:01 MST 1995
 *
 *	Description:	
 */
extern NhlErrorTypes _Nclfabs(
#if     NhlNeedProto
void
#endif
);


/*
 *      $Id: MathTemplate.h.sed,v 1.1 1995-01-31 22:25:56 ethan Exp $
 */
/************************************************************************
*									*
*			     Copyright (C)  1995			*
*	     University Corporation for Atmospheric Research		*
*			     All Rights Reserved			*
*									*
************************************************************************/
/*
 *	File:		
 *
 *	Author:		Ethan Alpert
 *			National Center for Atmospheric Research
 *			PO 3000, Boulder, Colorado
 *
 *	Date:		Tue Jan 31 15:17:01 MST 1995
 *
 *	Description:	
 */
extern NhlErrorTypes _Ncllog(
#if     NhlNeedProto
void
#endif
);


/*
 *      $Id: MathTemplate.h.sed,v 1.1 1995-01-31 22:25:56 ethan Exp $
 */
/************************************************************************
*									*
*			     Copyright (C)  1995			*
*	     University Corporation for Atmospheric Research		*
*			     All Rights Reserved			*
*									*
************************************************************************/
/*
 *	File:		
 *
 *	Author:		Ethan Alpert
 *			National Center for Atmospheric Research
 *			PO 3000, Boulder, Colorado
 *
 *	Date:		Tue Jan 31 15:17:01 MST 1995
 *
 *	Description:	
 */
extern NhlErrorTypes _Nclexp(
#if     NhlNeedProto
void
#endif
);


/*
 *      $Id: MathTemplate.h.sed,v 1.1 1995-01-31 22:25:56 ethan Exp $
 */
/************************************************************************
*									*
*			     Copyright (C)  1995			*
*	     University Corporation for Atmospheric Research		*
*			     All Rights Reserved			*
*									*
************************************************************************/
/*
 *	File:		
 *
 *	Author:		Ethan Alpert
 *			National Center for Atmospheric Research
 *			PO 3000, Boulder, Colorado
 *
 *	Date:		Tue Jan 31 15:17:01 MST 1995
 *
 *	Description:	
 */
extern NhlErrorTypes _Ncllog10(
#if     NhlNeedProto
void
#endif
);


/*
 *      $Id: MathTemplate.h.sed,v 1.1 1995-01-31 22:25:56 ethan Exp $
 */
/************************************************************************
*									*
*			     Copyright (C)  1995			*
*	     University Corporation for Atmospheric Research		*
*			     All Rights Reserved			*
*									*
************************************************************************/
/*
 *	File:		
 *
 *	Author:		Ethan Alpert
 *			National Center for Atmospheric Research
 *			PO 3000, Boulder, Colorado
 *
 *	Date:		Tue Jan 31 15:17:01 MST 1995
 *
 *	Description:	
 */
extern NhlErrorTypes _Nclsqrt(
#if     NhlNeedProto
void
#endif
);


/*
 *      $Id: MathTemplate.h.sed,v 1.1 1995-01-31 22:25:56 ethan Exp $
 */
/************************************************************************
*									*
*			     Copyright (C)  1995			*
*	     University Corporation for Atmospheric Research		*
*			     All Rights Reserved			*
*									*
************************************************************************/
/*
 *	File:		
 *
 *	Author:		Ethan Alpert
 *			National Center for Atmospheric Research
 *			PO 3000, Boulder, Colorado
 *
 *	Date:		Tue Jan 31 15:17:01 MST 1995
 *
 *	Description:	
 */
extern NhlErrorTypes _Nclatan2(
#if     NhlNeedProto
void
#endif
);

#endif /* MATHFUNC_h */ 
