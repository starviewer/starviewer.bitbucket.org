/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.3"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     EOLN = 258,
     ENDOFIF = 259,
     INT = 260,
     BYTE1 = 261,
     INTEGER2 = 262,
     INTEGER4 = 263,
     INTEGER8 = 264,
     REAL4 = 265,
     REAL8 = 266,
     DIMENSION = 267,
     DATA = 268,
     SAVE = 269,
     COMMON = 270,
     IMPLICIT = 271,
     WPARAM = 272,
     FNAME = 273,
     UNDEF = 274,
     SUBROUTINE = 275,
     FUNCTION = 276,
     NEW = 277,
     IN = 278,
     OUT = 279,
     INOUT = 280,
     TYPE = 281,
     NOMISSING = 282,
     DIMSIZES = 283,
     PROCEDURE = 284,
     MISSING = 285,
     RETURN = 286,
     REAL = 287,
     PRECISION = 288,
     INTEGER = 289,
     FLOAT = 290,
     LONG = 291,
     DOUBLE = 292,
     BYTE = 293,
     CHARACTER = 294,
     STRNG = 295,
     SHORT = 296,
     LOGICAL = 297,
     FARG = 298,
     FUNC = 299,
     SUBR = 300,
     PROC = 301,
     XFARG = 302,
     CVAR = 303
   };
#endif
/* Tokens.  */
#define EOLN 258
#define ENDOFIF 259
#define INT 260
#define BYTE1 261
#define INTEGER2 262
#define INTEGER4 263
#define INTEGER8 264
#define REAL4 265
#define REAL8 266
#define DIMENSION 267
#define DATA 268
#define SAVE 269
#define COMMON 270
#define IMPLICIT 271
#define WPARAM 272
#define FNAME 273
#define UNDEF 274
#define SUBROUTINE 275
#define FUNCTION 276
#define NEW 277
#define IN 278
#define OUT 279
#define INOUT 280
#define TYPE 281
#define NOMISSING 282
#define DIMSIZES 283
#define PROCEDURE 284
#define MISSING 285
#define RETURN 286
#define REAL 287
#define PRECISION 288
#define INTEGER 289
#define FLOAT 290
#define LONG 291
#define DOUBLE 292
#define BYTE 293
#define CHARACTER 294
#define STRNG 295
#define SHORT 296
#define LOGICAL 297
#define FARG 298
#define FUNC 299
#define SUBR 300
#define PROC 301
#define XFARG 302
#define CVAR 303




/* Copy the first part of user declarations.  */
#line 1 "wrapit.y"

#include <stdio.h>
#include <ncarg/hlu/hlu.h>
#include <ncarg/hlu/NresDB.h>
#include <ncarg/ncl/defs.h>
#include <ncarg/ncl/NclDataDefs.h>
#include "WSymbol.h"
#include "fstrings.h"
extern char *yytext;
void yyerror(const char *);
extern char* gettxt();
typedef struct wrap_src_node_list {
	void *node;
	struct wrap_src_node_list *next;
}WrapSrcListNode;

typedef struct int_list {
	int val;
	struct int_list *next;
}IntList;
extern char *wrapname;
int nargs;
int w_nargs;
extern WWrapRec *current;

WParamLoc* theparam = NULL;

WrapSrcListNode *WMakeListNode(void) 
{
	WrapSrcListNode *tmp = (WrapSrcListNode*)malloc(sizeof(WrapSrcListNode));
	tmp->next = NULL;
	tmp->node = NULL;
	return(tmp);
}

extern WCentry *WNewVDef(char* ,int , int ,char* ,int);
extern WGetArgValue *WNewArgVal();
extern WCallRec* WNewCallRec(char* ,int );
extern WSrcList* WNewAdditionalSrc(char*,int);
extern WParamLoc* NewParamLoc(NclSymbol*);

extern void DoTotal(NclSymbol*);
extern void DoDimsizes(NclSymbol*);


int FType(char *fname) {
	switch(fname[0]) {
	case 'i':
	case 'I':
	case 'j':
	case 'J':
	case 'k':
	case 'K':
	case 'l':
	case 'L':
	case 'm':
	case 'M':
	case 'n':
	case 'N':
		return(NCL_int);
	default:
		return(NCL_float);
	}
}
char *NType(int datatype) {
	switch(datatype) {
	case NCL_byte:
		return("NCL_byte");
	case NCL_char:
		return("NCL_char");
	case NCL_float:
		return("NCL_float");
	case NCL_double:
		return("NCL_double");
	case NCL_int:
		return("NCL_int");
	case NCL_short:
		return("NCL_short");
	case NCL_long: 
		return("NCL_long");
	case NCL_none:
		return("NclANY");
	case NCL_string: 
		return("NrmQuark");
	default:
		return("void");
	}
}
char *CType(int datatype) {
	switch(datatype) {
	case NCL_byte:
		return("byte");
	case NCL_char:
		return("char");
	case NCL_float:
		return("float");
	case NCL_double:
		return("double");
	case NCL_int:
		return("int");
	case NCL_short:
		return("short");
	case NCL_long: 
		return("long");
	case NCL_string: 
		return("NrmQuark");
	case NCL_none:
	default:
		return("void");
	}
}




/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 115 "wrapit.y"
{
	int integer;
	char *str;
	struct _NclSymbol *sym;
	struct _DimVal *dim;
	struct _FTypeVal *ftyp;
	void *src_node;
	WParamLoc  *ploc;
	WDSpecList *dsp;
	WModList *mod;
	struct int_list *ilist;
	struct wrap_src_node_list *list;
}
/* Line 193 of yacc.c.  */
#line 321 "y.tab.c"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 216 of yacc.c.  */
#line 334 "y.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int i)
#else
static int
YYID (i)
    int i;
#endif
{
  return i;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  25
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   263

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  62
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  36
/* YYNRULES -- Number of rules.  */
#define YYNRULES  131
/* YYNRULES -- Number of states.  */
#define YYNSTATES  241

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   303

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,    14,     2,     2,     4,     2,
       7,     8,     9,     2,     6,     2,     2,    13,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     5,     2,
       2,    12,     2,     2,     3,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    10,     2,    11,    15,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     6,    10,    13,    16,    20,    21,    26,
      35,    43,    50,    56,    57,    59,    61,    64,    66,    70,
      71,    73,    77,    82,    84,    86,    88,    91,    93,    95,
      97,    99,   101,   105,   107,   113,   117,   121,   125,   129,
     131,   135,   137,   141,   147,   152,   157,   159,   164,   166,
     170,   172,   174,   176,   177,   179,   180,   182,   184,   192,
     200,   206,   213,   215,   219,   221,   225,   228,   233,   237,
     241,   246,   251,   253,   255,   257,   259,   261,   263,   265,
     267,   269,   272,   276,   278,   279,   283,   287,   292,   297,
     305,   308,   312,   313,   317,   323,   327,   332,   337,   345,
     347,   349,   352,   355,   361,   365,   370,   375,   383,   387,
     389,   392,   395,   401,   403,   406,   409,   415,   416,   418,
     421,   425,   429,   433,   435,   437,   439,   443,   447,   452,
     457,   460
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      63,     0,    -1,    64,    17,    -1,    63,    64,    17,    -1,
      66,    71,    -1,    67,    71,    -1,    66,    71,    80,    -1,
      -1,    67,    71,    65,    80,    -1,    78,    34,    32,    79,
       7,    70,     8,    68,    -1,    78,    34,    32,    79,     7,
       8,    68,    -1,    33,    32,     7,    70,     8,    68,    -1,
      33,    32,     7,     8,    68,    -1,    -1,    69,    -1,    16,
      -1,    69,    16,    -1,    32,    -1,    32,     6,    70,    -1,
      -1,    72,    -1,    73,    74,    68,    -1,    72,    73,    74,
      68,    -1,    51,    -1,    47,    -1,    55,    -1,    50,    46,
      -1,    45,    -1,    26,    -1,    29,    -1,    27,    -1,    25,
      -1,    52,     9,    18,    -1,    52,    -1,    52,     9,     7,
       9,     8,    -1,    51,     9,    18,    -1,    47,     9,    18,
      -1,    55,     9,    18,    -1,    45,     9,    18,    -1,    75,
      -1,    74,     6,    75,    -1,    56,    -1,    56,     9,    18,
      -1,    56,     9,     7,     9,     8,    -1,    56,     7,    76,
       8,    -1,    32,    13,    18,    13,    -1,    32,    -1,    32,
       7,    76,     8,    -1,    77,    -1,    76,     6,    77,    -1,
      56,    -1,    18,    -1,     9,    -1,    -1,    73,    -1,    -1,
       9,    -1,    18,    -1,    42,    32,     7,    81,     8,    16,
      89,    -1,    34,    32,     7,    81,     8,    16,    85,    -1,
      42,    32,     7,     8,    16,    -1,    34,    32,     7,     8,
      16,    85,    -1,    82,    -1,    81,     6,    82,    -1,    32,
      -1,    32,     5,    84,    -1,    32,    83,    -1,    32,    83,
       5,    84,    -1,    10,    18,    11,    -1,    10,     9,    11,
      -1,    83,    10,     9,    11,    -1,    83,    10,    18,    11,
      -1,    48,    -1,    49,    -1,    47,    -1,    54,    -1,    50,
      -1,    52,    -1,    51,    -1,    53,    -1,    55,    -1,    86,
      16,    -1,    85,    86,    16,    -1,    90,    -1,    -1,    88,
      87,    95,    -1,    44,     5,    30,    -1,    44,     5,    13,
      30,    -1,    44,     5,    14,    30,    -1,    44,     5,    13,
      30,    10,    18,    11,    -1,    90,    16,    -1,    89,    90,
      16,    -1,    -1,    92,    91,    95,    -1,    60,     5,    35,
      12,    93,    -1,    60,     5,    30,    -1,    60,     5,    14,
      30,    -1,    60,     5,    13,    30,    -1,    60,     5,    13,
      30,    10,    18,    11,    -1,    18,    -1,    30,    -1,    14,
      30,    -1,    13,    30,    -1,    13,    30,    10,    18,    11,
      -1,    93,     6,    30,    -1,    93,     6,    14,    30,    -1,
      93,     6,    13,    30,    -1,    93,     6,    13,    30,    10,
      18,    11,    -1,    93,     6,    18,    -1,    30,    -1,    14,
      30,    -1,    13,    30,    -1,    13,    30,    10,    18,    11,
      -1,    60,    -1,    14,    60,    -1,    13,    60,    -1,    13,
      60,    10,    18,    11,    -1,    -1,    96,    -1,     5,    97,
      -1,    96,     5,    97,    -1,    43,    12,    94,    -1,    41,
      12,    94,    -1,    36,    -1,    37,    -1,    38,    -1,    39,
      12,    84,    -1,    39,    12,    94,    -1,     3,    32,    12,
      94,    -1,     4,    32,    12,    94,    -1,     3,    32,    -1,
       4,    32,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   154,   154,   276,   395,   530,   652,   655,   655,   660,
     690,   705,   728,   738,   740,   744,   747,   751,   770,   792,
     794,   799,   818,   834,   839,   845,   851,   857,   863,   869,
     875,   881,   887,   892,   898,   904,   922,   946,   964,   986,
     996,  1008,  1014,  1020,  1026,  1050,  1054,  1058,  1064,  1070,
    1078,  1084,  1090,  1099,  1102,  1113,  1115,  1118,  1123,  1141,
    1158,  1165,  1174,  1180,  1187,  1204,  1233,  1276,  1324,  1330,
    1336,  1342,  1350,  1351,  1352,  1353,  1354,  1355,  1356,  1357,
    1358,  1362,  1368,  1376,  1378,  1378,  1383,  1417,  1420,  1423,
    1443,  1449,  1457,  1457,  1495,  1545,  1568,  1588,  1608,  1643,
    1650,  1657,  1667,  1676,  1686,  1694,  1704,  1713,  1723,  1759,
    1766,  1776,  1785,  1795,  1798,  1801,  1804,  1825,  1828,  1834,
    1839,  1846,  1850,  1941,  1945,  1949,  1953,  1959,  1982,  1986,
    1990,  1994
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "'@'", "'&'", "':'", "','", "'('", "')'",
  "'*'", "'['", "']'", "'='", "'/'", "'#'", "'^'", "EOLN", "ENDOFIF",
  "INT", "BYTE1", "INTEGER2", "INTEGER4", "INTEGER8", "REAL4", "REAL8",
  "DIMENSION", "DATA", "SAVE", "COMMON", "IMPLICIT", "WPARAM", "FNAME",
  "UNDEF", "SUBROUTINE", "FUNCTION", "NEW", "IN", "OUT", "INOUT", "TYPE",
  "NOMISSING", "DIMSIZES", "PROCEDURE", "MISSING", "RETURN", "REAL",
  "PRECISION", "INTEGER", "FLOAT", "LONG", "DOUBLE", "BYTE", "CHARACTER",
  "STRNG", "SHORT", "LOGICAL", "FARG", "FUNC", "SUBR", "PROC", "XFARG",
  "CVAR", "$accept", "interface_list", "interface", "@1", "function_def",
  "subroutine_def", "oEOLN_list", "EOLN_list", "arg_list", "odata_defs",
  "data_defs", "ftype", "name_list", "param_def", "dim_list", "dim",
  "otyp", "len", "wrap_interface", "arg_dec_list", "declaration",
  "dim_size_list", "datatype", "farg_map_list", "farg_map", "@2", "rparam",
  "parg_map_list", "normal_param", "@3", "param_loc", "dspec_list",
  "vspec", "omods", "mods_list", "mod", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,    64,    38,    58,    44,    40,    41,    42,
      91,    93,    61,    47,    35,    94,   258,   259,   260,   261,
     262,   263,   264,   265,   266,   267,   268,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,   281,
     282,   283,   284,   285,   286,   287,   288,   289,   290,   291,
     292,   293,   294,   295,   296,   297,   298,   299,   300,   301,
     302,   303
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    62,    63,    63,    64,    64,    64,    65,    64,    66,
      66,    67,    67,    68,    68,    69,    69,    70,    70,    71,
      71,    72,    72,    73,    73,    73,    73,    73,    73,    73,
      73,    73,    73,    73,    73,    73,    73,    73,    73,    74,
      74,    75,    75,    75,    75,    75,    75,    75,    76,    76,
      77,    77,    77,    78,    78,    79,    79,    79,    80,    80,
      80,    80,    81,    81,    82,    82,    82,    82,    83,    83,
      83,    83,    84,    84,    84,    84,    84,    84,    84,    84,
      84,    85,    85,    86,    87,    86,    88,    88,    88,    88,
      89,    89,    91,    90,    92,    92,    92,    92,    92,    93,
      93,    93,    93,    93,    93,    93,    93,    93,    93,    94,
      94,    94,    94,    94,    94,    94,    94,    95,    95,    96,
      96,    97,    97,    97,    97,    97,    97,    97,    97,    97,
      97,    97
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     2,     3,     2,     2,     3,     0,     4,     8,
       7,     6,     5,     0,     1,     1,     2,     1,     3,     0,
       1,     3,     4,     1,     1,     1,     2,     1,     1,     1,
       1,     1,     3,     1,     5,     3,     3,     3,     3,     1,
       3,     1,     3,     5,     4,     4,     1,     4,     1,     3,
       1,     1,     1,     0,     1,     0,     1,     1,     7,     7,
       5,     6,     1,     3,     1,     3,     2,     4,     3,     3,
       4,     4,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     2,     3,     1,     0,     3,     3,     4,     4,     7,
       2,     3,     0,     3,     5,     3,     4,     4,     7,     1,
       1,     2,     2,     5,     3,     4,     4,     7,     3,     1,
       2,     2,     5,     1,     2,     2,     5,     0,     1,     2,
       3,     3,     3,     1,     1,     1,     3,     3,     4,     4,
       2,     2
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
      53,    31,    28,    30,    29,     0,    27,    24,     0,    23,
      33,    25,    53,     0,    19,    19,    54,     0,     0,     0,
       0,    26,     0,     0,     0,     1,     0,     2,     4,    20,
       0,     7,     0,     0,    38,    36,    35,     0,    32,    37,
       3,     0,     0,     6,     0,    46,    41,    13,    39,     0,
      55,    13,    17,     0,     0,     0,     0,    13,     0,     0,
       0,     0,     0,    15,    21,    14,     8,    56,    57,     0,
      12,     0,    13,    34,     0,     0,    22,    52,    51,    50,
       0,    48,     0,     0,     0,    42,    40,    16,     0,    18,
      11,     0,    64,     0,    62,     0,     0,     0,    47,    45,
      44,     0,    13,     0,     0,     0,     0,    66,     0,     0,
      60,     0,    49,    43,    10,    13,     0,     0,    61,     0,
      84,    83,    92,    74,    72,    73,    76,    78,    77,    79,
      75,    80,    65,     0,     0,     0,     0,    63,     0,     0,
       9,     0,     0,     0,    81,   117,   117,    69,    68,    67,
       0,     0,    59,    58,     0,     0,     0,    86,     0,     0,
      95,     0,    82,     0,    85,   118,    93,    70,    71,     0,
      90,    87,    88,    97,    96,     0,     0,     0,   123,   124,
     125,     0,     0,     0,   119,     0,    91,     0,     0,     0,
       0,    99,   100,    94,   130,   131,     0,     0,     0,   120,
       0,     0,   102,   101,     0,     0,     0,     0,     0,   109,
     113,   126,   127,   122,   121,    89,    98,     0,     0,     0,
     108,   104,   128,   129,   111,   115,   110,   114,     0,   106,
     105,     0,     0,   103,     0,     0,     0,     0,   112,   116,
     107
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    12,    13,    49,    14,    15,    64,    65,    53,    28,
      29,    16,    47,    48,    80,    81,    17,    69,    43,    93,
      94,   107,   132,   118,   119,   145,   120,   153,   121,   146,
     122,   193,   212,   164,   165,   184
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -135
static const yytype_int16 yypact[] =
{
      48,  -135,  -135,  -135,  -135,   -17,    27,    50,    25,    67,
      85,   110,     2,    74,    57,    57,  -135,    91,   119,   111,
     115,  -135,   134,    -2,   135,  -135,   120,  -135,   -23,    57,
     -22,   137,   136,    53,  -135,  -135,  -135,   157,  -135,  -135,
    -135,   138,   139,  -135,   -22,    10,   150,   105,  -135,   -23,
      79,   153,   166,   165,   167,   169,   170,   105,     4,   160,
       4,    51,   -22,  -135,  -135,   158,  -135,  -135,  -135,   172,
    -135,   148,   153,  -135,    55,    81,  -135,  -135,  -135,  -135,
     152,  -135,   168,   155,   173,  -135,  -135,  -135,    82,  -135,
    -135,   171,   145,   156,  -135,   174,   159,     4,  -135,  -135,
    -135,   175,   153,   176,   -30,    94,   121,   146,   154,   177,
    -135,   178,  -135,  -135,  -135,   153,   180,   183,   -30,   179,
    -135,  -135,  -135,  -135,  -135,  -135,  -135,  -135,  -135,  -135,
    -135,  -135,  -135,   181,   185,    94,   122,  -135,   -30,   129,
    -135,   104,    66,   182,  -135,   186,   186,  -135,  -135,  -135,
     188,   189,   -30,   129,   187,   184,   190,  -135,   191,   192,
    -135,   193,  -135,    29,  -135,   196,  -135,  -135,  -135,   194,
    -135,   197,  -135,   198,  -135,    92,   195,   199,  -135,  -135,
    -135,   200,   201,   203,  -135,    29,  -135,   205,   206,   202,
     204,  -135,  -135,   210,   207,   213,   -10,    -5,    -5,  -135,
     215,   217,   208,  -135,   102,    -5,    -5,   -12,    -4,  -135,
    -135,  -135,  -135,  -135,  -135,  -135,  -135,   211,   209,   212,
    -135,  -135,  -135,  -135,   220,   223,  -135,  -135,   224,   226,
    -135,   219,   222,  -135,   225,   227,   230,   233,  -135,  -135,
    -135
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -135,  -135,   234,  -135,  -135,  -135,   -51,  -135,   -64,   232,
    -135,   109,   162,   140,   144,   100,  -135,  -135,   214,   142,
     101,  -135,  -134,    73,  -106,  -135,  -135,  -135,   -61,  -135,
    -135,  -135,   -70,    99,  -135,    63
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -6
static const yytype_int16 yytable[] =
{
      70,   149,    25,   207,   208,    37,    76,    89,   207,   208,
      45,    41,   143,    77,   116,    18,    38,    58,   224,    42,
     209,    90,    78,    59,   103,   209,   226,     1,     2,     3,
     117,     4,   176,   177,    46,     5,    19,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   143,     6,   225,     7,
     210,   114,     8,     9,    10,   210,   227,    11,    84,    20,
      79,    51,   211,    91,   140,   178,   179,   180,   181,    85,
     182,    21,   183,     1,     2,     3,    22,     4,   154,   158,
     159,     5,     1,     2,     3,    52,     4,    92,    67,    95,
     102,    27,   169,     6,    23,     7,   160,    68,     8,     9,
      10,   161,     6,    11,     7,   189,   190,     8,     9,    10,
     191,    62,    11,    92,    52,   218,   219,   155,   156,    24,
     220,    63,   192,    30,    30,    32,    33,   213,   214,    34,
     133,   150,   221,    35,   157,   222,   223,    40,    44,   134,
     151,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     105,   135,    36,    39,    -5,   106,   136,    60,    97,    61,
      98,    97,   108,   100,   109,   108,    54,   111,    50,    63,
      55,    56,    71,    72,    87,    73,    74,    75,    82,    88,
      52,    99,   101,   113,   115,   141,    92,   104,   142,   117,
     110,   163,   147,   138,   139,   144,   148,   112,   162,   167,
     168,   185,    86,   170,    83,   175,    57,   187,   188,   137,
     186,   152,   196,   197,   171,   198,   204,    96,   217,   205,
     172,   173,   174,   200,   201,   206,   215,   194,   216,   228,
     231,   195,   202,   232,   203,   233,   234,   235,   238,   229,
     236,   239,   230,   237,   240,   166,    26,    31,   199,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    66
};

static const yytype_int16 yycheck[] =
{
      51,   135,     0,    13,    14,     7,    57,    71,    13,    14,
      32,    34,   118,     9,    44,    32,    18,     7,    30,    42,
      30,    72,    18,    13,    88,    30,    30,    25,    26,    27,
      60,    29,     3,     4,    56,    33,     9,    47,    48,    49,
      50,    51,    52,    53,    54,    55,   152,    45,    60,    47,
      60,   102,    50,    51,    52,    60,    60,    55,     7,     9,
      56,     8,   196,     8,   115,    36,    37,    38,    39,    18,
      41,    46,    43,    25,    26,    27,     9,    29,   139,    13,
      14,    33,    25,    26,    27,    32,    29,    32,     9,     8,
       8,    17,   153,    45,     9,    47,    30,    18,    50,    51,
      52,    35,    45,    55,    47,    13,    14,    50,    51,    52,
      18,     6,    55,    32,    32,    13,    14,    13,    14,     9,
      18,    16,    30,    14,    15,    34,     7,   197,   198,    18,
       9,     9,    30,    18,    30,   205,   206,    17,    29,    18,
      18,    47,    48,    49,    50,    51,    52,    53,    54,    55,
       5,     5,    18,    18,    17,    10,    10,     7,     6,     9,
       8,     6,     6,     8,     8,     6,     9,     8,    32,    16,
      32,    32,     6,     8,    16,     8,     7,     7,    18,     7,
      32,    13,     9,     8,     8,     5,    32,    16,     5,    60,
      16,     5,    11,    16,    16,    16,    11,    97,    16,    11,
      11,     5,    62,    16,    60,    12,    44,    10,    10,   108,
      16,   138,    12,    12,    30,    12,     6,    75,    10,    12,
      30,    30,    30,    18,    18,    12,    11,    32,    11,    18,
      10,    32,    30,    10,    30,    11,    10,    18,    11,    30,
      18,    11,    30,    18,    11,   146,    12,    15,   185,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    49
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    25,    26,    27,    29,    33,    45,    47,    50,    51,
      52,    55,    63,    64,    66,    67,    73,    78,    32,     9,
       9,    46,     9,     9,     9,     0,    64,    17,    71,    72,
      73,    71,    34,     7,    18,    18,    18,     7,    18,    18,
      17,    34,    42,    80,    73,    32,    56,    74,    75,    65,
      32,     8,    32,    70,     9,    32,    32,    74,     7,    13,
       7,     9,     6,    16,    68,    69,    80,     9,    18,    79,
      68,     6,     8,     8,     7,     7,    68,     9,    18,    56,
      76,    77,    18,    76,     7,    18,    75,    16,     7,    70,
      68,     8,    32,    81,    82,     8,    81,     6,     8,    13,
       8,     9,     8,    70,    16,     5,    10,    83,     6,     8,
      16,     8,    77,     8,    68,     8,    44,    60,    85,    86,
      88,    90,    92,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    84,     9,    18,     5,    10,    82,    16,    16,
      68,     5,     5,    86,    16,    87,    91,    11,    11,    84,
       9,    18,    85,    89,    90,    13,    14,    30,    13,    14,
      30,    35,    16,     5,    95,    96,    95,    11,    11,    90,
      16,    30,    30,    30,    30,    12,     3,     4,    36,    37,
      38,    39,    41,    43,    97,     5,    16,    10,    10,    13,
      14,    18,    30,    93,    32,    32,    12,    12,    12,    97,
      18,    18,    30,    30,     6,    12,    12,    13,    14,    30,
      60,    84,    94,    94,    94,    11,    11,    10,    13,    14,
      18,    30,    94,    94,    30,    60,    30,    60,    18,    30,
      30,    10,    10,    11,    10,    18,    18,    18,    11,    11,
      11
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *bottom, yytype_int16 *top)
#else
static void
yy_stack_print (bottom, top)
    yytype_int16 *bottom;
    yytype_int16 *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      fprintf (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      fprintf (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  
  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;
#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  yytype_int16 yyssa[YYINITDEPTH];
  yytype_int16 *yyss = yyssa;
  yytype_int16 *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     look-ahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to look-ahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 155 "wrapit.y"
    {
		WVDefList *c_vdefs;
		WArgValList *c_argval;
		WSrcList *additional_src;
		WCallRec *c_callrec;
		char buffer[BUFFSIZE];
		char upper[BUFFSIZE];
		char lower[BUFFSIZE];
		int i;


		c_callrec = current->c_callrec;
		fprintf(stdout,fdef_fmt,current->c_defstring);
		c_vdefs = current->c_vdefs;
		while(c_vdefs != NULL) {
			sprintf(buffer,"%s ",CType(c_vdefs->def->datatype));
			if(c_vdefs->def->isptr) {
				strcat(buffer,"*");
			}
			strcat(buffer,c_vdefs->def->string);
			if(c_vdefs->def->array_spec != NULL) {
				strcat(buffer,c_vdefs->def->array_spec);
			}
			fprintf(stdout,"\t%s;\n",buffer);
			c_vdefs = c_vdefs->next;
		}
		
		c_argval = current->c_argval;
		while(c_argval != NULL) {
			fprintf(stdout,argval_fmt,
				c_argval->arec->assign_to,
				CType(c_argval->arec->datatype),
				c_argval->arec->pnum,
				c_argval->arec->totalp,
				c_argval->arec->ndims==NULL?"NULL":c_argval->arec->ndims,
				c_argval->arec->dimsizes==NULL?"NULL":c_argval->arec->dimsizes,
				c_argval->arec->missing==NULL?"NULL":c_argval->arec->missing,
				c_argval->arec->hasmissing==NULL?"NULL":c_argval->arec->hasmissing,
				c_argval->arec->type==NULL?"NULL":c_argval->arec->type,
				c_argval->arec->rw);
			additional_src = c_argval->arec->additional_src;
			while(additional_src != NULL) {
				if(additional_src->src) {
					fprintf(stdout,"%s",additional_src->src);
				}
				additional_src = additional_src->next;
			}
			c_argval = c_argval->next;	
		}
		c_vdefs = current->c_vdefs;
		while(c_vdefs != NULL) {
			additional_src = c_vdefs->def->additional_src;
			while(additional_src != NULL) {
				if(additional_src->order == 0) {
					if(additional_src->src) {
						fprintf(stdout,"%s",additional_src->src);
					}
				}
				additional_src = additional_src->next;
			}
			c_vdefs = c_vdefs->next;
		}
		c_vdefs = current->c_vdefs;
		while(c_vdefs != NULL) {
			additional_src = c_vdefs->def->additional_src;
			while(additional_src != NULL) {
				if(additional_src->order == 1) {
					if(additional_src->src) {
						fprintf(stdout,"%s",additional_src->src);
					}
				}
				additional_src = additional_src->next;
			}
			c_vdefs = c_vdefs->next;
		}
		c_callrec = current->c_callrec;
		fprintf(stdout,"\t%s",c_callrec->callstring);
		sprintf(buffer,"(");
		for(i = 0; i < c_callrec->n_args;i++) {
			if(c_callrec->arg_strings[i] == NULL) {
				strcat(buffer,"NULL");
			} else if(!c_callrec->arg_strings[i]->isptr) {
				strcat(buffer,"&");
				strcat(buffer,c_callrec->arg_strings[i]->string);
			} else {
				strcat(buffer,c_callrec->arg_strings[i]->string);
			}
			if(i!= c_callrec->n_args-1) {
				strcat(buffer,",");
			} 
		}
		if(c_callrec->nstrs != 0) {
			strcat(buffer,",");
			for(;i < c_callrec->n_args+c_callrec->nstrs;i++) {
				strcat(buffer,c_callrec->arg_strings[i]->string);
				if(i!= c_callrec->n_args + c_callrec->nstrs -1) {
					strcat(buffer,",");
				} 
			}
		}
		strcat(buffer,");\n\n");
		fprintf(stdout,"%s",buffer);
		c_vdefs = current->c_vdefs;
		while(c_vdefs != NULL) {
			additional_src = c_vdefs->def->additional_src;
			while(additional_src != NULL) {
				if(additional_src->order == 2) {
					if(additional_src->src) {
						fprintf(stdout,"%s",additional_src->src);
					}
				}
				additional_src = additional_src->next;
			}
			c_vdefs = c_vdefs->next;
		}
		fprintf(stdout,"\t%s\n",current->rtrn);
		fprintf(stdout,"}\n");
		
		nargs = 0;
		w_nargs = 0;
	}
    break;

  case 3:
#line 277 "wrapit.y"
    {
		WVDefList *c_vdefs;
		WArgValList *c_argval;
		WSrcList *additional_src;
		WCallRec *c_callrec;
		char buffer[BUFFSIZE];
		char upper[BUFFSIZE];
		char lower[BUFFSIZE];
		int i;


		c_callrec = current->c_callrec;
		fprintf(stdout,fdef_fmt,current->c_defstring);
		c_vdefs = current->c_vdefs;
		while(c_vdefs != NULL) {
			sprintf(buffer,"%s ",CType(c_vdefs->def->datatype));
			if(c_vdefs->def->isptr) {
				strcat(buffer,"*");
			}
			strcat(buffer,c_vdefs->def->string);
			if(c_vdefs->def->array_spec != NULL) {
				strcat(buffer,c_vdefs->def->array_spec);
			}
			fprintf(stdout,"\t%s;\n",buffer);
			c_vdefs = c_vdefs->next;
		}
		
		c_argval = current->c_argval;
		while(c_argval != NULL) {
			fprintf(stdout,argval_fmt,
				c_argval->arec->assign_to,
				CType(c_argval->arec->datatype),
				c_argval->arec->pnum,
				c_argval->arec->totalp,
				c_argval->arec->ndims==NULL?"NULL":c_argval->arec->ndims,
				c_argval->arec->dimsizes==NULL?"NULL":c_argval->arec->dimsizes,
				c_argval->arec->missing==NULL?"NULL":c_argval->arec->missing,
				c_argval->arec->hasmissing==NULL?"NULL":c_argval->arec->hasmissing,
				c_argval->arec->type==NULL?"NULL":c_argval->arec->type,
				c_argval->arec->rw);
			if(c_argval->arec->additional_src != NULL) {
				fprintf(stdout,"%s",c_argval->arec->additional_src);
			}
			c_argval = c_argval->next;
		}
		c_vdefs = current->c_vdefs;
		while(c_vdefs != NULL) {
			additional_src = c_vdefs->def->additional_src;
			while(additional_src != NULL) {
				if(additional_src->order==0 ) {
					if(additional_src->src) {
						fprintf(stdout,"%s",additional_src->src);
					}
				} 
				additional_src = additional_src->next;
			}
			c_vdefs = c_vdefs->next;
		}
		c_vdefs = current->c_vdefs;
		while(c_vdefs != NULL) {
			additional_src = c_vdefs->def->additional_src;
			while(additional_src != NULL) {
				if(additional_src->order==1 ) {
					if(additional_src->src) {
						fprintf(stdout,"%s",additional_src->src);
					}
				} 
				additional_src = additional_src->next;
			}
			c_vdefs = c_vdefs->next;
		}
		c_callrec = current->c_callrec;
		fprintf(stdout,"\t%s",c_callrec->callstring);
		sprintf(buffer,"(");
		for(i = 0; i < c_callrec->n_args;i++) {
			if(c_callrec->arg_strings[i] == NULL) {
				strcat(buffer,"NULL");
			} else if(!c_callrec->arg_strings[i]->isptr) {
				strcat(buffer,"&");
				strcat(buffer,c_callrec->arg_strings[i]->string);
			} else {
				strcat(buffer,c_callrec->arg_strings[i]->string);
			}
		
			if((i!= c_callrec->n_args-1)) {
				strcat(buffer,",");
			} 
		}
		if(c_callrec->nstrs != 0) {
			strcat(buffer,",");
			for(;i < c_callrec->n_args+c_callrec->nstrs;i++) {
				strcat(buffer,c_callrec->arg_strings[i]->string);
				if(i!= c_callrec->n_args + c_callrec->nstrs -1) {
					strcat(buffer,",");
				} 
			}
		}
		strcat(buffer,");\n\n");
		fprintf(stdout,"%s",buffer);
		c_vdefs = current->c_vdefs;
		while(c_vdefs != NULL) {
			additional_src = c_vdefs->def->additional_src;
			while(additional_src != NULL) {
				if(additional_src->order == 2) {
					if(additional_src->src) {
						fprintf(stdout,"%s",additional_src->src);
					}
				}
				additional_src = additional_src->next;
			}
			c_vdefs = c_vdefs->next;
		}
		fprintf(stdout,"\t%s\n",current->rtrn);
		fprintf(stdout,"}\n");
		
		nargs = 0;
		w_nargs = 0;
	}
    break;

  case 4:
#line 396 "wrapit.y"
    {
/*
* No wrapper info, so output records have not been built.
*/
		int i,j,tmpj,nstrs;
		NclSymbol *s,*s0;
		NclSymbol *wp;
		char buffer[BUFFSIZE];
		char upper[BUFFSIZE];
		char lower[BUFFSIZE];
		WGetArgValue *arg;
		WCallRec *thecall;
		NclSymbol **tmp;
		WCentry *tmpc;
		WSrcList *tmp_src;

		thecall = WNewCallRec((yyvsp[(1) - (2)].sym)->name,nargs);
/*
* Now since its a one-one function the wrapper parameter information must
* be take from the FORTRAN information This list would otherwise be created
* already.
*/
		for(i = 0; i < strlen((yyvsp[(1) - (2)].sym)->name); i++) {
                        if((!isdigit((yyvsp[(1) - (2)].sym)->name[i]))&&(isalpha((yyvsp[(1) - (2)].sym)->name[i]))) {
                        	if(isupper((yyvsp[(1) - (2)].sym)->name[i])) {
                               		upper[i] = (yyvsp[(1) - (2)].sym)->name[i];
                               		lower[i] = (char)((int)(yyvsp[(1) - (2)].sym)->name[i] + 32);
                        	} else {
                                	lower[i] = (yyvsp[(1) - (2)].sym)->name[i];
                                	upper[i] = (char)((int)(yyvsp[(1) - (2)].sym)->name[i] - 32);
                        	}
			} else {
				upper[i] = lower[i] = (yyvsp[(1) - (2)].sym)->name[i];
			}
                }
		lower[i] = '\0';
		upper[i] = '\0';
		fprintf(stdout,"extern %s NGCALLF(%s,%s)();\n",CType((yyvsp[(1) - (2)].sym)->u.func->datatype),lower,upper);

		current->f_or_p = 1;
		current->c_defstring = malloc(strlen((yyvsp[(1) - (2)].sym)->name)+1);
		strcpy(current->c_defstring, (yyvsp[(1) - (2)].sym)->name);

		sprintf(buffer,"%s_ret",(yyvsp[(1) - (2)].sym)->name);
		tmpc = WNewVDef(buffer,(yyvsp[(1) - (2)].sym)->u.func->datatype,0,NULL,0);

		sprintf(buffer,"%s = NGCALLF(%s,%s)",tmpc->string,lower,upper);
		thecall->callstring = malloc(strlen(buffer)+1);
		strcpy(thecall->callstring,buffer);

		sprintf(buffer,"%s_ret_dimsizes",(yyvsp[(1) - (2)].sym)->name);
		(void)WNewVDef(buffer,NCL_long,0,"[NCL_MAX_DIMENSIONS]",0);
		

		for (i = 0; i < (yyvsp[(1) - (2)].sym)->u.func->n_args; i++) {
			s = (yyvsp[(1) - (2)].sym)->u.func->args[i];
/*
* Also since no wrapper only need to retrieve pointer to var
* no need for ndims,dimsizes,missing....
*/
			wp = _NclAddInScope(current->wrec,(char*)s->name,WPARAM);
			wp->u.wparam = (WParamInfo*)malloc(sizeof(WParamInfo));


			arg = WNewArgVal();
			if(s->u.farg->datatype == NCL_none) {
				s->u.farg->datatype = wp->u.wparam->datatype = FType(s->name);
			} else {
				wp->u.wparam->datatype = s->u.farg->datatype;
			}
			wp->u.wparam->n_dims = s->u.farg->n_dims;
			for(j = 0; j < s->u.farg->n_dims; j++) {
				wp->u.wparam->dim_sizes[j] = s->u.farg->dim_sizes[j];
			}
			arg->assign_to = s->name;
			arg->datatype = s->u.farg->datatype;
			arg->pnum = i;
			arg->totalp = (yyvsp[(1) - (2)].sym)->u.func->n_args;
			arg->rw = 1;
			arg->nd[0] = '\t';
			arg->nd[1] = ')';
			arg->nd[2] = ';';
			arg->nd[3] = '\n';
			arg->nd[4] = '\0';
			wp->u.wparam->getarg = arg;
			thecall->arg_strings[i] = WNewVDef(s->name,s->u.farg->datatype,1,NULL,0);
		}
/*
* This loop searches for string arguments and defines the length argument needed by FORTRAN
*/
		nstrs = 0;
		for (i = 0; i < (yyvsp[(1) - (2)].sym)->u.func->n_args; i++) {
			s = (yyvsp[(1) - (2)].sym)->u.func->args[i];
			if(s->u.farg->datatype == NCL_string) {
				sprintf(buffer,"%s_len",s->name);
				thecall->arg_strings[(yyvsp[(1) - (2)].sym)->u.func->n_args+nstrs] = WNewVDef(buffer,NCL_int,0,NULL,0);
				sprintf(buffer,"\t%s_len = strlen(NrmQuarkToString(*%s));\n",s->name,s->name);
				tmp_src = thecall->arg_strings[(yyvsp[(1) - (2)].sym)->u.func->n_args+nstrs]->additional_src;
				thecall->arg_strings[(yyvsp[(1) - (2)].sym)->u.func->n_args+nstrs]->additional_src = WNewAdditionalSrc(buffer,1);
				thecall->arg_strings[(yyvsp[(1) - (2)].sym)->u.func->n_args+nstrs]->additional_src->next = tmp_src;


				sprintf(buffer,"%s_str",s->name);
				thecall->arg_strings[i] = WNewVDef(buffer,NCL_char,1,NULL,1);
				sprintf(buffer,"\t%s_str = malloc(%s_len + 1);\n\tstrcpy(%s_str,NrmQuarkToString(*%s));\n",s->name,s->name,s->name,s->name);
				tmp_src = thecall->arg_strings[i]->additional_src;
				
				thecall->arg_strings[i]->additional_src = WNewAdditionalSrc(buffer,1);
				thecall->arg_strings[i]->additional_src->next = tmp_src;
				sprintf(buffer,"\t%s_str[%s_len] = \'\\0\';\n\t*%s = NrmStringToQuark(%s_str);\n\tfree(%s_str);\n",s->name,s->name,s->name,s->name,s->name);
				tmp_src = thecall->arg_strings[i]->additional_src;
				thecall->arg_strings[i]->additional_src = WNewAdditionalSrc(buffer,2);
				thecall->arg_strings[i]->additional_src->next = tmp_src;

				nstrs++;
			} else {
				for(j = 0; j < (yyvsp[(1) - (2)].sym)->u.func->args[i]->u.farg->n_dims; j++) {
					if((yyvsp[(1) - (2)].sym)->u.func->args[i]->u.farg->dim_refs[j] != NULL) {
						s0 = _NclLookUpInScope(current->wrec,(yyvsp[(1) - (2)].sym)->u.func->args[i]->name);
						DoDimsizes(s0);
						sprintf(buffer,"\tif(*%s != (int)%s_dimsizes[%d]) {\n\t\tNhlPError(NhlFATAL,NhlEUNKNOWN,\"%s: dimension size of dimension (%d) of %s must be equal to the value of %s\");\n\t\treturn(NhlFATAL);\n\t}\n",(yyvsp[(1) - (2)].sym)->u.func->args[i]->u.farg->dim_refs[j]->name,(yyvsp[(1) - (2)].sym)->u.func->args[i]->name,j,(yyvsp[(1) - (2)].sym)->name,j,(yyvsp[(1) - (2)].sym)->u.func->args[i]->name,(yyvsp[(1) - (2)].sym)->u.func->args[i]->u.farg->dim_refs[j]->name);
						tmp_src = thecall->arg_strings[i]->additional_src;
						thecall->arg_strings[i]->additional_src = WNewAdditionalSrc(buffer,1);
						thecall->arg_strings[i]->additional_src->next = tmp_src;
					}
				}
			}
		}
		thecall->nstrs = nstrs;
		sprintf(buffer,"%s_ret_dimsizes[0] = 1;\n\treturn(NclReturnValue(&%s_ret,1,%s_ret_dimsizes,NULL,%s,1));",(yyvsp[(1) - (2)].sym)->name,(yyvsp[(1) - (2)].sym)->name,(yyvsp[(1) - (2)].sym)->name,NType((yyvsp[(1) - (2)].sym)->u.func->datatype));
		current->rtrn = malloc(strlen(buffer)+1);
		strcpy(current->rtrn,buffer);
		
	}
    break;

  case 5:
#line 531 "wrapit.y"
    {
/*
* No wrapper info, so output records have not been built.
*/
		int i,j,nstrs,tmpj;
		NclSymbol *s,*s0;
		NclSymbol *wp;
		char buffer[BUFFSIZE];
		char lower[BUFFSIZE];
		char upper[BUFFSIZE];
		WGetArgValue *arg;
		WCallRec *thecall;
		NclSymbol **tmp;
		WSrcList *tmp_src;

		thecall = WNewCallRec((yyvsp[(1) - (2)].sym)->name,nargs);
/*
* Now since its a one-one function the wrapper parameter information must
* be take from the FORTRAN information This list would otherwise be created
* already.
*/
		for(i = 0; i < strlen((yyvsp[(1) - (2)].sym)->name); i++) {
			if((!isdigit((yyvsp[(1) - (2)].sym)->name[i]))&&(isalpha((yyvsp[(1) - (2)].sym)->name[i]))) {
                        	if(isupper((yyvsp[(1) - (2)].sym)->name[i])) {
                                	upper[i] = (yyvsp[(1) - (2)].sym)->name[i];
                                	lower[i] = (char)((int)(yyvsp[(1) - (2)].sym)->name[i] + 32);
                        	} else {
                                	lower[i] = (yyvsp[(1) - (2)].sym)->name[i];
                                	upper[i] = (char)((int)(yyvsp[(1) - (2)].sym)->name[i] - 32);
                        	}
			} else {
                                	upper[i] = lower[i] = (yyvsp[(1) - (2)].sym)->name[i];
			}
                }
		lower[i] = '\0';
		upper[i] = '\0';
		sprintf(buffer,"NGCALLF(%s,%s)",lower,upper);
		thecall->callstring = malloc(strlen(buffer)+1);
		strcpy(thecall->callstring,buffer);
		current->f_or_p = 0;
		current->c_defstring = malloc(strlen((yyvsp[(1) - (2)].sym)->name)+1);
		strcpy(current->c_defstring, (yyvsp[(1) - (2)].sym)->name);
		for (i = 0; i < (yyvsp[(1) - (2)].sym)->u.subr->n_args; i++) {
			s = (yyvsp[(1) - (2)].sym)->u.subr->args[i];
/*
* Also since no wrapper only need to retrieve pointer to var
* no need for ndims,dimsizes,missing....
*/
			wp = _NclAddInScope(current->wrec,(char*)s->name,WPARAM);
			wp->u.wparam = (WParamInfo*)malloc(sizeof(WParamInfo));


			arg = WNewArgVal();
			if(s->u.farg->datatype == NCL_none) {
				s->u.farg->datatype = wp->u.wparam->datatype = FType(s->name);
			} else {
				wp->u.wparam->datatype = s->u.farg->datatype;
			}
			wp->u.wparam->n_dims = s->u.farg->n_dims;
			for(j = 0; j < s->u.farg->n_dims; j++) {
				wp->u.wparam->dim_sizes[j] = s->u.farg->dim_sizes[j];
			}
			arg->assign_to = s->name;
			arg->datatype = s->u.farg->datatype;
			arg->pnum = i;
			arg->totalp = (yyvsp[(1) - (2)].sym)->u.subr->n_args;
			arg->rw = 1;
			arg->nd[0] = '\t';
			arg->nd[1] = ')';
			arg->nd[2] = ';';
			arg->nd[3] = '\n';
			arg->nd[4] = '\0';
			wp->u.wparam->getarg = arg;
			thecall->arg_strings[i] = WNewVDef(s->name,s->u.farg->datatype,1,NULL,0);
		}
/*
* This loop searches for string arguments and defines the length argument needed by FORTRAN
*/
		nstrs = 0;
		for (i = 0; i < (yyvsp[(1) - (2)].sym)->u.subr->n_args; i++) {
			s = (yyvsp[(1) - (2)].sym)->u.subr->args[i];
			if(s->u.farg->datatype == NCL_string) {
				sprintf(buffer,"%s_len",s->name);
				thecall->arg_strings[(yyvsp[(1) - (2)].sym)->u.subr->n_args+nstrs] = WNewVDef(buffer,NCL_int,0,NULL,0);
				sprintf(buffer,"\t%s_len = strlen(NrmQuarkToString(*%s));\n",s->name,s->name);
				tmp_src = thecall->arg_strings[(yyvsp[(1) - (2)].sym)->u.subr->n_args+nstrs]->additional_src;
				thecall->arg_strings[(yyvsp[(1) - (2)].sym)->u.subr->n_args+nstrs]->additional_src = WNewAdditionalSrc(buffer,1);
				thecall->arg_strings[(yyvsp[(1) - (2)].sym)->u.subr->n_args+nstrs]->additional_src->next = tmp_src;


				sprintf(buffer,"%s_str",s->name);
				thecall->arg_strings[i] = WNewVDef(buffer,NCL_char,1,NULL,1);
				sprintf(buffer,"\t%s_str = malloc(%s_len + 1);\n\tstrcpy(%s_str,NrmQuarkToString(*%s));\n",s->name,s->name,s->name,s->name);
				tmp_src = thecall->arg_strings[i]->additional_src;
				
				thecall->arg_strings[i]->additional_src = WNewAdditionalSrc(buffer,1);
				thecall->arg_strings[i]->additional_src->next = tmp_src;
				sprintf(buffer,"\t%s_str[%s_len] = \'\\0\';\n\t*%s = NrmStringToQuark(%s_str);\n\tfree(%s_str);\n",s->name,s->name,s->name,s->name,s->name);
				tmp_src = thecall->arg_strings[i]->additional_src;
				thecall->arg_strings[i]->additional_src = WNewAdditionalSrc(buffer,2);
				thecall->arg_strings[i]->additional_src->next = tmp_src;
				nstrs++;
			} else {
				for(j = 0; j < (yyvsp[(1) - (2)].sym)->u.subr->args[i]->u.farg->n_dims; j++) {
					if((yyvsp[(1) - (2)].sym)->u.subr->args[i]->u.farg->dim_refs[j] != NULL) {
						s0 = _NclLookUpInScope(current->wrec,(yyvsp[(1) - (2)].sym)->u.subr->args[i]->name);
						DoDimsizes(s0);
						sprintf(buffer,"\tif(*%s != (int)%s_dimsizes[%d]) {\n\t\tNhlPError(NhlFATAL,NhlEUNKNOWN,\"%s: dimension size of dimension (%d) of %s must be equal to the value of %s\");\n\t\treturn(NhlFATAL);\n\t}\n",(yyvsp[(1) - (2)].sym)->u.subr->args[i]->u.farg->dim_refs[j]->name,(yyvsp[(1) - (2)].sym)->u.subr->args[i]->name,j,(yyvsp[(1) - (2)].sym)->name,j,(yyvsp[(1) - (2)].sym)->u.subr->args[i]->name,(yyvsp[(1) - (2)].sym)->u.subr->args[i]->u.farg->dim_refs[j]->name);
						tmp_src = thecall->arg_strings[i]->additional_src;
						thecall->arg_strings[i]->additional_src = WNewAdditionalSrc(buffer,1);
						thecall->arg_strings[i]->additional_src->next = tmp_src;
					}
				}
			}
		}
		thecall->nstrs = nstrs;
		current->rtrn = malloc(strlen("return(NhlNOERROR);")+1);
		strcpy(current->rtrn,"return(NhlNOERROR);");
		
		
	}
    break;

  case 6:
#line 653 "wrapit.y"
    {
        }
    break;

  case 7:
#line 655 "wrapit.y"
    { (void)WNewCallRec((yyvsp[(1) - (2)].sym)->name,nargs); }
    break;

  case 8:
#line 656 "wrapit.y"
    {
        }
    break;

  case 9:
#line 661 "wrapit.y"
    {
		WrapSrcListNode* tmp,*tmp2;
		int count = 0;
		(yyvsp[(3) - (8)].sym)->type = FUNC;
		(yyvsp[(3) - (8)].sym)->u.func = (WFuncInfo*)malloc(sizeof(WFuncInfo));

                tmp = (yyvsp[(6) - (8)].list);
                while(tmp != NULL) {
                        count++;
                        tmp = tmp->next;
                }
		(yyvsp[(3) - (8)].sym)->u.func->n_args = count;
		(yyvsp[(3) - (8)].sym)->u.func->args = (NclSymbol**)malloc(sizeof(NclSymbol*)*count);
		if((yyvsp[(1) - (8)].ftyp) == NULL) {
			(yyvsp[(3) - (8)].sym)->u.func->datatype = FType((yyvsp[(3) - (8)].sym)->name);
		} else {
			(yyvsp[(3) - (8)].sym)->u.func->datatype = (yyvsp[(1) - (8)].ftyp)->datatype;
		}
		tmp = (yyvsp[(6) - (8)].list);
		for(count = 0; count < (yyvsp[(3) - (8)].sym)->u.func->n_args; count++) {
			(yyvsp[(3) - (8)].sym)->u.func->args[count] = tmp->node;
			tmp2 = tmp;
			tmp = tmp->next;
			free(tmp2);
		}
		(yyval.sym) = (yyvsp[(3) - (8)].sym);
		

	}
    break;

  case 10:
#line 691 "wrapit.y"
    {
		(yyvsp[(3) - (7)].sym)->type = FUNC;
		(yyvsp[(3) - (7)].sym)->u.func = (WFuncInfo*)malloc(sizeof(WFuncInfo));
		(yyvsp[(3) - (7)].sym)->u.func->n_args = 0;
		(yyvsp[(3) - (7)].sym)->u.func->args = NULL;
		if((yyvsp[(1) - (7)].ftyp) == NULL) {
			(yyvsp[(3) - (7)].sym)->u.func->datatype = FType((yyvsp[(3) - (7)].sym)->name);
		} else {
			(yyvsp[(3) - (7)].sym)->u.func->datatype = (yyvsp[(1) - (7)].ftyp)->datatype;
		}
		(yyval.sym) = (yyvsp[(3) - (7)].sym);
	}
    break;

  case 11:
#line 706 "wrapit.y"
    {
		WrapSrcListNode* tmp,*tmp2;
		int count = 0;
		(yyvsp[(2) - (6)].sym)->type = SUBR;
		(yyvsp[(2) - (6)].sym)->u.subr = (WSubrInfo*)malloc(sizeof(WSubrInfo));
	
		tmp = (yyvsp[(4) - (6)].list);
		while(tmp != NULL) {
			count++;
			tmp = tmp->next;
		}
		(yyvsp[(2) - (6)].sym)->u.subr->args = (NclSymbol**)malloc(sizeof(NclSymbol*)*count);
		(yyvsp[(2) - (6)].sym)->u.subr->n_args = count;
		tmp = (yyvsp[(4) - (6)].list);
		for( count = 0; count < (yyvsp[(2) - (6)].sym)->u.subr->n_args; count++ ) {
			(yyvsp[(2) - (6)].sym)->u.subr->args[count] = tmp->node;
			tmp2 =tmp;
			tmp = tmp->next;
			free(tmp2);
		}
		(yyval.sym) = (yyvsp[(2) - (6)].sym);
	}
    break;

  case 12:
#line 729 "wrapit.y"
    {
		(yyvsp[(2) - (5)].sym)->type = SUBR;
		(yyvsp[(2) - (5)].sym)->u.subr = (WSubrInfo*)malloc(sizeof(WSubrInfo));
		(yyvsp[(2) - (5)].sym)->u.subr->args = NULL;
		(yyvsp[(2) - (5)].sym)->u.subr->n_args = 0;
		(yyval.sym) = (yyvsp[(2) - (5)].sym);
	}
    break;

  case 13:
#line 738 "wrapit.y"
    {
	}
    break;

  case 14:
#line 741 "wrapit.y"
    {
	}
    break;

  case 15:
#line 745 "wrapit.y"
    {
	}
    break;

  case 16:
#line 748 "wrapit.y"
    {
	}
    break;

  case 17:
#line 752 "wrapit.y"
    {
		NclSymbol *s;
		char buffer[10];
		(yyvsp[(1) - (1)].sym)->type = FARG;
		(yyvsp[(1) - (1)].sym)->u.farg = (WFargInfo*)malloc(sizeof(WFargInfo));
		(yyvsp[(1) - (1)].sym)->u.farg->arg_num = nargs;
		(yyvsp[(1) - (1)].sym)->u.farg->dim_sizes[0] = 1;
		(yyvsp[(1) - (1)].sym)->u.farg->dim_refs[0] = NULL;
		(yyvsp[(1) - (1)].sym)->u.farg->n_dims = 1;
		sprintf(buffer,"FARG%d",nargs++);
#if  YYDEBUG
#endif
		s = _NclAddInScope(current->wrec,buffer,XFARG);
		s->u.xref = (yyvsp[(1) - (1)].sym);
		(yyval.list) = WMakeListNode();
		(yyval.list)->node = (yyvsp[(1) - (1)].sym);
		(yyval.list)->next = NULL;
	}
    break;

  case 18:
#line 771 "wrapit.y"
    {
		NclSymbol *s;
		char buffer[BUFFSIZE];
		(yyvsp[(1) - (3)].sym)->u.farg = (WFargInfo*)malloc(sizeof(WFargInfo));
		(yyvsp[(1) - (3)].sym)->type = FARG;
		(yyvsp[(1) - (3)].sym)->u.farg->arg_num = nargs;
		(yyvsp[(1) - (3)].sym)->u.farg->dim_sizes[0] = 1;
		(yyvsp[(1) - (3)].sym)->u.farg->dim_refs[0] = NULL;
		(yyvsp[(1) - (3)].sym)->u.farg->n_dims = 1;
		sprintf(buffer,"FARG%d",nargs++);
#if  YYDEBUG
#endif

		s = _NclAddInScope(current->wrec,buffer,XFARG);
		s->u.xref = (yyvsp[(1) - (3)].sym);
		(yyval.list) = WMakeListNode();
		(yyval.list)->node = (yyvsp[(1) - (3)].sym);
		(yyval.list)->next = (yyvsp[(3) - (3)].list);
	}
    break;

  case 19:
#line 792 "wrapit.y"
    {
	}
    break;

  case 20:
#line 795 "wrapit.y"
    {
	}
    break;

  case 21:
#line 800 "wrapit.y"
    {
		WrapSrcListNode * tmp,*tmp2;
/*
*------> This is where problems with MD char arrays happens
*/
		tmp = (yyvsp[(2) - (3)].list);
		while(tmp != NULL) {
			tmp2 = tmp;
			((NclSymbol*)tmp->node)->u.farg->datatype = (yyvsp[(1) - (3)].ftyp)->datatype;
			if(((yyvsp[(1) - (3)].ftyp)->datatype == NCL_char)&&((yyvsp[(1) - (3)].ftyp)->size != -1)) {
				((NclSymbol*)tmp->node)->u.farg->dim_sizes[0] = (yyvsp[(1) - (3)].ftyp)->size;
				((NclSymbol*)tmp->node)->u.farg->dim_refs[0] = NULL;
			}
			tmp = tmp->next;
			free(tmp2);
		}

	}
    break;

  case 22:
#line 818 "wrapit.y"
    {
		WrapSrcListNode * tmp,*tmp2;

		tmp = (yyvsp[(3) - (4)].list);
		while(tmp != NULL) {
			tmp2 = tmp;
			((NclSymbol*)tmp->node)->u.farg->datatype = (yyvsp[(2) - (4)].ftyp)->datatype;
			if(((yyvsp[(2) - (4)].ftyp)->datatype == NCL_char)&&((yyvsp[(2) - (4)].ftyp)->size != -1)) {
				((NclSymbol*)tmp->node)->u.farg->dim_sizes[0] = (yyvsp[(2) - (4)].ftyp)->size;
				((NclSymbol*)tmp->node)->u.farg->dim_refs[0] = NULL;
			}
			tmp = tmp->next;
			free(tmp2);
		}
	}
    break;

  case 23:
#line 835 "wrapit.y"
    {	(yyval.ftyp) = (FTypeVal*)malloc(sizeof(FTypeVal));
		(yyval.ftyp)->datatype = NCL_byte;
		(yyval.ftyp)->size = -1;
	}
    break;

  case 24:
#line 840 "wrapit.y"
    {
		(yyval.ftyp) = (FTypeVal*)malloc(sizeof(FTypeVal));
		(yyval.ftyp)->datatype = NCL_int;
		(yyval.ftyp)->size = -1;
	}
    break;

  case 25:
#line 846 "wrapit.y"
    {
		(yyval.ftyp) = (FTypeVal*)malloc(sizeof(FTypeVal));
		(yyval.ftyp)->datatype = NCL_logical;
		(yyval.ftyp)->size = -1;
	}
    break;

  case 26:
#line 852 "wrapit.y"
    {
		(yyval.ftyp) = (FTypeVal*)malloc(sizeof(FTypeVal));
		(yyval.ftyp)->datatype = NCL_double;
		(yyval.ftyp)->size = -1;
	}
    break;

  case 27:
#line 858 "wrapit.y"
    {
		(yyval.ftyp) = (FTypeVal*)malloc(sizeof(FTypeVal));
		(yyval.ftyp)->datatype = NCL_float;
		(yyval.ftyp)->size = -1;
	}
    break;

  case 28:
#line 864 "wrapit.y"
    {
		(yyval.ftyp) = (FTypeVal*)malloc(sizeof(FTypeVal));
		(yyval.ftyp)->datatype = NCL_none;
		(yyval.ftyp)->size = -1;
	}
    break;

  case 29:
#line 870 "wrapit.y"
    {
		(yyval.ftyp) = (FTypeVal*)malloc(sizeof(FTypeVal));
		(yyval.ftyp)->datatype = NCL_none;
		(yyval.ftyp)->size = -1;
	}
    break;

  case 30:
#line 876 "wrapit.y"
    {
		(yyval.ftyp) = (FTypeVal*)malloc(sizeof(FTypeVal));
		(yyval.ftyp)->datatype = NCL_none;
		(yyval.ftyp)->size = -1;
	}
    break;

  case 31:
#line 882 "wrapit.y"
    {
		(yyval.ftyp) = (FTypeVal*)malloc(sizeof(FTypeVal));
		(yyval.ftyp)->datatype = NCL_none;
		(yyval.ftyp)->size = -1;
	}
    break;

  case 32:
#line 887 "wrapit.y"
    {
		(yyval.ftyp) = (FTypeVal*)malloc(sizeof(FTypeVal));
		(yyval.ftyp)->datatype =  NCL_char;
		(yyval.ftyp)->size = (yyvsp[(3) - (3)].integer);
	}
    break;

  case 33:
#line 893 "wrapit.y"
    {
		(yyval.ftyp) = (FTypeVal*)malloc(sizeof(FTypeVal));
		(yyval.ftyp)->datatype =  NCL_char;
		(yyval.ftyp)->size = -1;
	}
    break;

  case 34:
#line 899 "wrapit.y"
    {
		(yyval.ftyp) = (FTypeVal*)malloc(sizeof(FTypeVal));
		(yyval.ftyp)->datatype = NCL_string;
		(yyval.ftyp)->size = -1;
	}
    break;

  case 35:
#line 905 "wrapit.y"
    {
		(yyval.ftyp) = (FTypeVal*)malloc(sizeof(FTypeVal));
		(yyval.ftyp)->size = -1;
		switch((yyvsp[(3) - (3)].integer)) {
		case 1:
			(yyval.ftyp)->datatype = NCL_byte;
			break;
		case 2:
			(yyval.ftyp)->datatype = NCL_short;
			break;
		case 4:
			(yyval.ftyp)->datatype = NCL_int;
			break;
		default:
			(yyval.ftyp)->datatype = NCL_none;
		}
	}
    break;

  case 36:
#line 923 "wrapit.y"
    {
		(yyval.ftyp) = (FTypeVal*)malloc(sizeof(FTypeVal));
		(yyval.ftyp)->size = -1;
		switch((yyvsp[(3) - (3)].integer)){
		case 1:
			(yyval.ftyp)->datatype = NCL_byte;
			break;
		case 2:
			(yyval.ftyp)->datatype = NCL_short;
			break;
		case 4:
			(yyval.ftyp)->datatype = NCL_int;
			break;
		case 8:
			if(sizeof(long)==8) {
				(yyval.ftyp)->datatype = NCL_long;
			} else {
				(yyval.ftyp)->datatype = NCL_none;
			}
		default:
			(yyval.ftyp) = NCL_none;
		}
	}
    break;

  case 37:
#line 947 "wrapit.y"
    {
		(yyval.ftyp) = (FTypeVal*)malloc(sizeof(FTypeVal));
		(yyval.ftyp)->size = -1;
		switch((yyvsp[(3) - (3)].integer)){
		case 1:
			(yyval.ftyp)->datatype = NCL_byte;
			break;
		case 2:
			(yyval.ftyp)->datatype = NCL_short;
			break;
		case 4:
			(yyval.ftyp)->datatype = NCL_logical;
			break;
		default:
			(yyval.ftyp)->datatype = NCL_none;
		}
	}
    break;

  case 38:
#line 965 "wrapit.y"
    {
		(yyval.ftyp) = (FTypeVal*)malloc(sizeof(FTypeVal));
		(yyval.ftyp)->size = -1;
		switch((yyvsp[(3) - (3)].integer)){
		case 1:
		case 2:
			(yyval.ftyp)->datatype = NCL_none;
			break;
		case 4:
			(yyval.ftyp)->datatype = NCL_float;
			break;
		case 8:
			(yyval.ftyp)->datatype = NCL_double;
			break;
		default:
			(yyval.ftyp) = NCL_none;
		}
	}
    break;

  case 39:
#line 987 "wrapit.y"
    {
		if((yyvsp[(1) - (1)].sym) != NULL) {
			(yyval.list) = (WrapSrcListNode*) malloc(sizeof(WrapSrcListNode));
			(yyval.list)->node = (yyvsp[(1) - (1)].sym);
			(yyval.list)->next = NULL;
		} else {
			(yyval.list) = NULL;
		}
	}
    break;

  case 40:
#line 997 "wrapit.y"
    {
		if((yyvsp[(3) - (3)].sym) != NULL) {
			(yyval.list) = (WrapSrcListNode*) malloc(sizeof(WrapSrcListNode));
			(yyval.list)->node = (yyvsp[(3) - (3)].sym);
			(yyval.list)->next = (yyvsp[(1) - (3)].list);
		} else {
			(yyval.list) = (yyvsp[(1) - (3)].list);
		}
	}
    break;

  case 41:
#line 1009 "wrapit.y"
    {
		(yyvsp[(1) - (1)].sym)->u.farg->n_dims = 1;
		(yyvsp[(1) - (1)].sym)->u.farg->dim_sizes[0] = 1;
		(yyval.sym) = (yyvsp[(1) - (1)].sym);
	}
    break;

  case 42:
#line 1015 "wrapit.y"
    {
		(yyvsp[(1) - (3)].sym)->u.farg->n_dims = 1;
		(yyvsp[(1) - (3)].sym)->u.farg->dim_sizes[0] = (yyvsp[(3) - (3)].integer);
		(yyval.sym) = (yyvsp[(1) - (3)].sym);
	}
    break;

  case 43:
#line 1021 "wrapit.y"
    {
		(yyvsp[(1) - (5)].sym)->u.farg->n_dims = 1;
		(yyvsp[(1) - (5)].sym)->u.farg->dim_sizes[0] = -1;
		(yyval.sym) = (yyvsp[(1) - (5)].sym);
	}
    break;

  case 44:
#line 1027 "wrapit.y"
    {
		int n_dims=0;
		int i;
		WrapSrcListNode* tmp;
		tmp = (yyvsp[(3) - (4)].list);
		while(tmp != NULL) {
			n_dims++;
			tmp = tmp->next;
		}
		(yyvsp[(1) - (4)].sym)->u.farg->n_dims = n_dims;
		tmp = (yyvsp[(3) - (4)].list);
		for(i = 0; i < n_dims; i++) {
			if(((DimVal*)tmp->node)->kind) {
				(yyvsp[(1) - (4)].sym)->u.farg->dim_sizes[i]  = -1;
				(yyvsp[(1) - (4)].sym)->u.farg->dim_refs[i] = ((DimVal*)tmp->node)->u.sym;
			} else {
				(yyvsp[(1) - (4)].sym)->u.farg->dim_refs[i]  = NULL;
				(yyvsp[(1) - (4)].sym)->u.farg->dim_sizes[i] = ((DimVal*)tmp->node)->u.val;
			}
			tmp = tmp->next;
		}
		(yyval.sym) = (yyvsp[(1) - (4)].sym);
	}
    break;

  case 45:
#line 1051 "wrapit.y"
    {
		(yyval.sym) = NULL;
	}
    break;

  case 46:
#line 1055 "wrapit.y"
    {
		(yyval.sym) = NULL;
	}
    break;

  case 47:
#line 1059 "wrapit.y"
    {
		(yyval.sym) = NULL;
	}
    break;

  case 48:
#line 1065 "wrapit.y"
    {
		(yyval.list) = (WrapSrcListNode*)malloc(sizeof(WrapSrcListNode));
		(yyval.list)->node = (yyvsp[(1) - (1)].dim);
		(yyval.list)->next = NULL;
	}
    break;

  case 49:
#line 1071 "wrapit.y"
    {
		(yyval.list) = (WrapSrcListNode*)malloc(sizeof(WrapSrcListNode));
		(yyval.list)->node = (yyvsp[(3) - (3)].dim);
		(yyval.list)->next = (yyvsp[(1) - (3)].list);
	}
    break;

  case 50:
#line 1079 "wrapit.y"
    {
		(yyval.dim) = (DimVal*)malloc(sizeof(DimVal));
		(yyval.dim)->kind = 1;
		(yyval.dim)->u.sym = (yyvsp[(1) - (1)].sym);
	}
    break;

  case 51:
#line 1085 "wrapit.y"
    {
		(yyval.dim) = (DimVal*)malloc(sizeof(DimVal));
		(yyval.dim)->kind = 0;
		(yyval.dim)->u.val = (yyvsp[(1) - (1)].integer);
	}
    break;

  case 52:
#line 1091 "wrapit.y"
    {
		(yyval.dim) = (DimVal*)malloc(sizeof(DimVal));
		(yyval.dim)->kind = 0;
		(yyval.dim)->u.val = -1;
	}
    break;

  case 53:
#line 1099 "wrapit.y"
    {
		(yyval.ftyp) = NULL;
	}
    break;

  case 54:
#line 1103 "wrapit.y"
    {
		if((yyvsp[(1) - (1)].ftyp)->datatype == NCL_none) {
			fprintf(stderr,"Unsupported FORTRAN type detected\n");
		}
		(yyval.ftyp) = (yyvsp[(1) - (1)].ftyp);
	}
    break;

  case 55:
#line 1113 "wrapit.y"
    {
	}
    break;

  case 56:
#line 1116 "wrapit.y"
    {
	}
    break;

  case 57:
#line 1119 "wrapit.y"
    {
	}
    break;

  case 58:
#line 1124 "wrapit.y"
    {
		WrapSrcListNode* tmp;
		NclSymbol *s;

		tmp = (yyvsp[(4) - (7)].list);

		while(tmp!= NULL) {
			s = (NclSymbol*)tmp->node;		
			s->u.wparam->getarg->totalp = w_nargs;
			tmp = tmp->next;
		}
		(yyvsp[(2) - (7)].sym)->type = PROC;
		current->f_or_p = 0;
		current->c_defstring = malloc(strlen((yyvsp[(2) - (7)].sym)->name)+1);
		strcpy(current->c_defstring,(yyvsp[(2) - (7)].sym)->name);

	}
    break;

  case 59:
#line 1142 "wrapit.y"
    {
		WrapSrcListNode* tmp;
		NclSymbol *s;

		tmp = (yyvsp[(4) - (7)].list);

		while(tmp!= NULL) {
			s = (NclSymbol*)tmp->node;		
			s->u.wparam->getarg->totalp = w_nargs;
			tmp = tmp->next;
		}
		(yyvsp[(2) - (7)].sym)->type = FUNC;
		current->f_or_p = 1;
		current->c_defstring = malloc(strlen((yyvsp[(2) - (7)].sym)->name)+1);
		strcpy(current->c_defstring,(yyvsp[(2) - (7)].sym)->name);
	}
    break;

  case 60:
#line 1159 "wrapit.y"
    {
		(yyvsp[(2) - (5)].sym)->type = PROC;
		current->f_or_p = 0;
		current->c_defstring = malloc(strlen((yyvsp[(2) - (5)].sym)->name)+1);
		strcpy(current->c_defstring,(yyvsp[(2) - (5)].sym)->name);
	}
    break;

  case 61:
#line 1166 "wrapit.y"
    {
		(yyvsp[(2) - (6)].sym)->type = FUNC;
		current->f_or_p = 1;
		current->c_defstring = malloc(strlen((yyvsp[(2) - (6)].sym)->name)+1);
		strcpy(current->c_defstring,(yyvsp[(2) - (6)].sym)->name);
	}
    break;

  case 62:
#line 1175 "wrapit.y"
    {
		(yyval.list) = (WrapSrcListNode*)malloc(sizeof(WrapSrcListNode));
		(yyval.list)->next = NULL;
		(yyval.list)->node = (void*)(yyvsp[(1) - (1)].sym);
	}
    break;

  case 63:
#line 1181 "wrapit.y"
    {
		(yyval.list) = (WrapSrcListNode*)malloc(sizeof(WrapSrcListNode));
		(yyval.list)->next = (yyvsp[(1) - (3)].list);
		(yyval.list)->node = (void*)(yyvsp[(3) - (3)].sym);
	}
    break;

  case 64:
#line 1187 "wrapit.y"
    {
		WGetArgValue *tmp;
		char buffer[BUFFSIZE];
		(yyvsp[(1) - (1)].sym)->type = WPARAM;	
		(yyvsp[(1) - (1)].sym)->u.wparam = malloc(sizeof(WParamInfo));
		(yyvsp[(1) - (1)].sym)->u.wparam->n_dims = -1;
		(yyvsp[(1) - (1)].sym)->u.wparam->dim_sizes[0] = -1;
		(yyvsp[(1) - (1)].sym)->u.wparam->datatype = -1;
		(yyvsp[(1) - (1)].sym)->u.wparam->getarg = tmp = WNewArgVal();
		tmp->assign_to = (yyvsp[(1) - (1)].sym)->name;
		tmp->pnum = w_nargs;
		tmp->datatype = -1;
		tmp->rw = 1;
		(yyval.sym) = (yyvsp[(1) - (1)].sym);
		(void)WNewVDef((yyvsp[(1) - (1)].sym)->name,-1,1,NULL,0);
		w_nargs++;
	}
    break;

  case 65:
#line 1205 "wrapit.y"
    {
		WGetArgValue *tmp;
		WSrcList *srclist;
		char buffer[BUFFSIZE];

		(yyvsp[(1) - (3)].sym)->type = WPARAM;
		(yyvsp[(1) - (3)].sym)->u.wparam = malloc(sizeof(WParamInfo));
		(yyvsp[(1) - (3)].sym)->u.wparam->n_dims = -1;
		(yyvsp[(1) - (3)].sym)->u.wparam->dim_sizes[0] = -1;
		(yyvsp[(1) - (3)].sym)->u.wparam->datatype = (yyvsp[(3) - (3)].integer);
		(yyvsp[(1) - (3)].sym)->u.wparam->getarg = tmp = WNewArgVal();
		(void)WNewVDef((yyvsp[(1) - (3)].sym)->name,(yyvsp[(3) - (3)].integer),1,NULL,0);

		tmp->assign_to = (yyvsp[(1) - (3)].sym)->name;
		tmp->pnum = w_nargs;
		tmp->datatype = (yyvsp[(3) - (3)].integer);
		tmp->rw = 1;
                sprintf(buffer,"&%s_type",(yyvsp[(1) - (3)].sym)->name);
                tmp->type = malloc(strlen(buffer)+1);
                strcpy(tmp->type,buffer);


		sprintf(buffer,"%s_type",(yyvsp[(1) - (3)].sym)->name);
		(void)WNewVDef(buffer,NCL_int,0,NULL,0);

		(yyval.sym) = (yyvsp[(1) - (3)].sym);
		w_nargs++;
	}
    break;

  case 66:
#line 1234 "wrapit.y"
    {	
		WGetArgValue *tmp;
		char buffer[BUFFSIZE];
		IntList *itmp;
		int i = 0 ;

		(yyvsp[(1) - (2)].sym)->type = WPARAM;
		(yyvsp[(1) - (2)].sym)->u.wparam = malloc(sizeof(WParamInfo));
		itmp = (yyvsp[(2) - (2)].ilist);
		while(itmp != NULL) {
			(yyvsp[(1) - (2)].sym)->u.wparam->dim_sizes[i] = itmp->val;	
			i++;
			itmp = itmp->next;
		}
		(yyvsp[(1) - (2)].sym)->u.wparam->n_dims = i;
		(yyvsp[(1) - (2)].sym)->u.wparam->datatype = -1;
		(yyvsp[(1) - (2)].sym)->u.wparam->getarg = tmp = WNewArgVal();
		sprintf(buffer,"%s",(yyvsp[(1) - (2)].sym)->name);
		(void)WNewVDef(buffer,-1,1,NULL,0);

		tmp->assign_to = (yyvsp[(1) - (2)].sym)->name;
		tmp->pnum = w_nargs;
		tmp->datatype = -1;
		tmp->rw = 1;

		sprintf(buffer,"&%s_ndims",(yyvsp[(1) - (2)].sym)->name);
		tmp->ndims = malloc(strlen(buffer)+1);
		strcpy(tmp->ndims,buffer);


		sprintf(buffer,"%s_ndims",(yyvsp[(1) - (2)].sym)->name);
		(void)WNewVDef(buffer,NCL_int,0,NULL,0);

		sprintf(buffer,"%s_dimsizes",(yyvsp[(1) - (2)].sym)->name);
		(void)WNewVDef(buffer,NCL_long,0,"[NCL_MAX_DIMENSIONS]",0);
		tmp->dimsizes = malloc(strlen(buffer)+1);
		strcpy(tmp->dimsizes,buffer);


		(yyval.sym) = (yyvsp[(1) - (2)].sym);
		w_nargs++;
	}
    break;

  case 67:
#line 1277 "wrapit.y"
    {	
		WGetArgValue *tmp;
		char buffer[BUFFSIZE];
		IntList *itmp;
		int i = 0 ;
		(yyvsp[(1) - (4)].sym)->type = WPARAM;
		(yyvsp[(1) - (4)].sym)->u.wparam = malloc(sizeof(WParamInfo));
		itmp = (yyvsp[(2) - (4)].ilist);
		while(itmp != NULL) {
			(yyvsp[(1) - (4)].sym)->u.wparam->dim_sizes[i] = itmp->val;	
			i++;
			itmp = itmp->next;
		}
		(yyvsp[(1) - (4)].sym)->u.wparam->n_dims = i;
		(yyvsp[(1) - (4)].sym)->u.wparam->datatype = (yyvsp[(4) - (4)].integer);
		(yyvsp[(1) - (4)].sym)->u.wparam->getarg = tmp = WNewArgVal();
		sprintf(buffer,"%s",(yyvsp[(1) - (4)].sym)->name);
		(void)WNewVDef(buffer,(yyvsp[(4) - (4)].integer),1,NULL,0);

		tmp->assign_to = (yyvsp[(1) - (4)].sym)->name;
		tmp->pnum = w_nargs;
		tmp->datatype = (yyvsp[(4) - (4)].integer);
		tmp->rw = 1;

		sprintf(buffer,"&%s_ndims",(yyvsp[(1) - (4)].sym)->name);	
		tmp->ndims = malloc(strlen(buffer)+1);
		strcpy(tmp->ndims,buffer);
		sprintf(buffer,"%s_ndims",(yyvsp[(1) - (4)].sym)->name);
		(void)WNewVDef(buffer,NCL_int,0,NULL,0);


		sprintf(buffer,"%s_dimsizes",(yyvsp[(1) - (4)].sym)->name);
		tmp->dimsizes= malloc(strlen(buffer)+1);
		strcpy(tmp->dimsizes,buffer);
		(void)WNewVDef(buffer,NCL_long,0,"[NCL_MAX_DIMENSIONS]",0);

		sprintf(buffer,"&%s_type",(yyvsp[(1) - (4)].sym)->name);
		tmp->type = malloc(strlen(buffer)+1);
		strcpy(tmp->type,buffer);

		sprintf(buffer,"%s_type",(yyvsp[(1) - (4)].sym)->name);
		(void)WNewVDef(buffer,NCL_int,0,NULL,0);

		(yyval.sym) = (yyvsp[(1) - (4)].sym);
		w_nargs++;
	}
    break;

  case 68:
#line 1325 "wrapit.y"
    {
                (yyval.ilist) = (IntList*) malloc(sizeof(IntList));
                (yyval.ilist)->val = (yyvsp[(2) - (3)].integer);
                (yyval.ilist)->next = NULL;
	}
    break;

  case 69:
#line 1331 "wrapit.y"
    {
                (yyval.ilist) = (IntList*) malloc(sizeof(IntList));
                (yyval.ilist)->val = -1;
                (yyval.ilist)->next = NULL;
	}
    break;

  case 70:
#line 1337 "wrapit.y"
    {
                (yyval.ilist) = (IntList*) malloc(sizeof(IntList));
                (yyval.ilist)->val = -1;
                (yyval.ilist)->next = (yyvsp[(1) - (4)].ilist);
	}
    break;

  case 71:
#line 1343 "wrapit.y"
    {
                (yyval.ilist) = (IntList*) malloc(sizeof(IntList));
                (yyval.ilist)->val = (yyvsp[(3) - (4)].integer);
                (yyval.ilist)->next = (yyvsp[(1) - (4)].ilist);
	}
    break;

  case 72:
#line 1350 "wrapit.y"
    { (yyval.integer) = NCL_float; }
    break;

  case 73:
#line 1351 "wrapit.y"
    { (yyval.integer) = NCL_long; }
    break;

  case 74:
#line 1352 "wrapit.y"
    { (yyval.integer) = NCL_int; }
    break;

  case 75:
#line 1353 "wrapit.y"
    { (yyval.integer) = NCL_short; }
    break;

  case 76:
#line 1354 "wrapit.y"
    { (yyval.integer) = NCL_double; }
    break;

  case 77:
#line 1355 "wrapit.y"
    { (yyval.integer) = NCL_char; }
    break;

  case 78:
#line 1356 "wrapit.y"
    { (yyval.integer) = NCL_byte; }
    break;

  case 79:
#line 1357 "wrapit.y"
    { (yyval.integer) = NCL_string; }
    break;

  case 80:
#line 1358 "wrapit.y"
    { (yyval.integer) = NCL_logical; }
    break;

  case 81:
#line 1363 "wrapit.y"
    {
		(yyval.list) = (WrapSrcListNode*) malloc(sizeof(WrapSrcListNode));
		(yyval.list)->node = (void*)(yyvsp[(1) - (2)].src_node);
		(yyval.list)->next = NULL;
	}
    break;

  case 82:
#line 1369 "wrapit.y"
    {
		(yyval.list) = (WrapSrcListNode*) malloc(sizeof(WrapSrcListNode));
		(yyval.list)->node = (void*)(yyvsp[(2) - (3)].src_node);
		(yyval.list)->next = (yyvsp[(1) - (3)].list);
	}
    break;

  case 83:
#line 1376 "wrapit.y"
    {
	}
    break;

  case 84:
#line 1378 "wrapit.y"
    {theparam = (yyvsp[(1) - (1)].ploc);}
    break;

  case 85:
#line 1378 "wrapit.y"
    {
		
	}
    break;

  case 86:
#line 1384 "wrapit.y"
    {
		char buffer[BUFFSIZE];
		WParamLoc *loc = (WParamLoc*)malloc(sizeof(WParamLoc));
		NclSymbol *s = _NclLookUpInScope(current->crec,(yyvsp[(3) - (3)].sym)->name);
		int i;

		loc->type = RETURNPARAM;
		loc->datatype = (yyvsp[(3) - (3)].sym)->u.wparam->datatype;
		loc->xfarg = NULL;
		loc->cdef = s->u.centry;
		loc->wsym = (yyvsp[(3) - (3)].sym);
		
		if((yyvsp[(3) - (3)].sym)->u.wparam->n_dims == -1) {
			loc->typeofdim = 1;
			loc->n_dims = 1;
			sprintf(buffer,"%s_dimsizes",(yyvsp[(3) - (3)].sym)->name);	
			s = _NclLookUpInScope(current->crec,buffer);
			loc->altdimsref = s->u.centry;
			sprintf(loc->altndimref,"%s_ndims",(yyvsp[(3) - (3)].sym)->name);
		} else {
			loc->typeofdim = 0;
			loc->n_dims = (yyvsp[(3) - (3)].sym)->u.wparam->n_dims;
			for(i = 0; i < (yyvsp[(3) - (3)].sym)->u.wparam->n_dims; i++) {
				loc->dim_sizes[i] = (yyvsp[(3) - (3)].sym)->u.wparam->dim_sizes[i];
				if(loc->dim_sizes[i] = -1) {
					sprintf(loc->dim_refs[i],"(%s_dimsizes[%d])",(yyvsp[(3) - (3)].sym)->name,i);
				} else {
					sprintf(loc->dim_refs[i],"(%d)",loc->dim_sizes[i]);
				}
			}
		}
		(yyval.ploc) = loc;
	}
    break;

  case 87:
#line 1418 "wrapit.y"
    {
	}
    break;

  case 88:
#line 1421 "wrapit.y"
    {
	}
    break;

  case 89:
#line 1424 "wrapit.y"
    {
        }
    break;

  case 90:
#line 1444 "wrapit.y"
    {
		(yyval.list) = (WrapSrcListNode*) malloc(sizeof(WrapSrcListNode));
		(yyval.list)->node = (void*)(yyvsp[(1) - (2)].src_node);
		(yyval.list)->next = NULL;
	}
    break;

  case 91:
#line 1450 "wrapit.y"
    {
		(yyval.list) = (WrapSrcListNode*) malloc(sizeof(WrapSrcListNode));
		(yyval.list)->node = (void*)(yyvsp[(2) - (3)].src_node);
		(yyval.list)->next = (yyvsp[(1) - (3)].list);
	}
    break;

  case 92:
#line 1457 "wrapit.y"
    { theparam = (yyvsp[(1) - (1)].ploc); }
    break;

  case 93:
#line 1458 "wrapit.y"
    {
		char buffer[BUFFSIZE];
		int i,j;
		WSrcList *tmp;
		WModList *mods;


		switch((yyvsp[(1) - (3)].ploc)->type) {
		case FARGNEW:
			current->c_callrec->arg_strings[(yyvsp[(1) - (3)].ploc)->xfarg->u.xref->u.farg->arg_num] = (yyvsp[(1) - (3)].ploc)->cdef;
			sprintf(buffer,"\t%s = (%s*)malloc(sizeof(%s)",(yyvsp[(1) - (3)].ploc)->cdef->string,CType((yyvsp[(1) - (3)].ploc)->datatype),CType((yyvsp[(1) - (3)].ploc)->datatype));
			for(i = 0; i < (yyvsp[(1) - (3)].ploc)->n_dims; i++) {
				strcat(buffer," * ");
				strcat(buffer,(yyvsp[(1) - (3)].ploc)->dim_refs[i]);
			}
			strcat(buffer,");\n");
			if((yyvsp[(1) - (3)].ploc)->cdef->additional_src == NULL) {
				(yyvsp[(1) - (3)].ploc)->cdef->additional_src = WNewAdditionalSrc(buffer,1);
			} else {
				tmp =  WNewAdditionalSrc(buffer,1);
				tmp->next = (yyvsp[(1) - (3)].ploc)->cdef->additional_src;
				(yyvsp[(1) - (3)].ploc)->cdef->additional_src = tmp;
			}
			break;
		case PARAMVAL:
		case TOTALPARAM:
			current->c_callrec->arg_strings[(yyvsp[(1) - (3)].ploc)->xfarg->u.xref->u.farg->arg_num] = (yyvsp[(1) - (3)].ploc)->cdef;
			break;
		case DIMSPARAM:
			break;
		case DIMINDEXPARAM:
			break;
		}

	}
    break;

  case 94:
#line 1496 "wrapit.y"
    {
		WParamLoc *loc = NewParamLoc((yyvsp[(1) - (5)].sym));
		char buffer[BUFFSIZE];
		WDSpecList *tmp = (yyvsp[(5) - (5)].dsp);
		WCentry *ctmp;
		int j;
	
		loc->type = FARGNEW;
		loc->xfarg = (yyvsp[(1) - (5)].sym);
		loc->cdef = WNewVDef((yyvsp[(1) - (5)].sym)->name,(yyvsp[(1) - (5)].sym)->u.xref->u.farg->datatype,1,NULL,1);
		sprintf(loc->call_string,"%s",(yyvsp[(1) - (5)].sym)->name);

		loc->n_dims = 0	;
		while(tmp != NULL) {

			switch(tmp->type) {
			case INTDIMSIZES:
				sprintf(loc->dim_refs[loc->n_dims],"(%d)",tmp->u.val);
				loc->n_dims++;
				break;
			case PARAMVALDIMSIZES:
				sprintf(loc->dim_refs[loc->n_dims],"(*%s)",tmp->u.pval.wsym->name);
				loc->n_dims++;
				break;
			case TOTALPARAMDIMSIZES:
				sprintf(loc->dim_refs[loc->n_dims],"(%s_total)",tmp->u.tpval.wsym->name);
				loc->n_dims++;
				break;
			case DIMSPARAMDIMSIZES:
				if(tmp->u.dpval.wsym->u.wparam->n_dims == -1) {
					DoTotal(tmp->u.dpval.wsym);
					sprintf(loc->dim_refs[loc->n_dims],"(%s_total)",tmp->u.dpval.wsym->name);
					loc->n_dims++;
				} else {
					for(j = 0; j < tmp->u.dpval.wsym->u.wparam->n_dims; j++) {
						sprintf(loc->dim_refs[loc->n_dims],"(%s_dimsizes[%d])",tmp->u.dpval.wsym->name,j);	
						loc->n_dims++;
					}
				}
				break;
			case DIMINDEXPARAMDIMSIZES:
				sprintf(loc->dim_refs[loc->n_dims],"(%s_dimsizes[%d])",tmp->u.dipval.wsym->name,tmp->u.dipval.index);	
				loc->n_dims++;
				break;
			}
			tmp = tmp->next;
		}
		(yyval.ploc) = loc;
	}
    break;

  case 95:
#line 1546 "wrapit.y"
    {

		WParamLoc *loc = NewParamLoc((yyvsp[(1) - (3)].sym));
		NclSymbol *tmp;
		int i;
		char buffer[BUFFSIZE];

		DoDimsizes((yyvsp[(3) - (3)].sym));
		loc->type = PARAMVAL;
		loc->xfarg = (yyvsp[(1) - (3)].sym);
		tmp = _NclLookUpInScope(current->crec,(yyvsp[(3) - (3)].sym)->name);
		loc->cdef = tmp->u.centry;
		sprintf(loc->call_string,"%s",(yyvsp[(3) - (3)].sym)->name);
		loc->n_dims = (yyvsp[(3) - (3)].sym)->u.wparam->n_dims;
		for(i = 0; i < (yyvsp[(3) - (3)].sym)->u.wparam->n_dims; i++) {
			loc->dim_sizes[i] = (yyvsp[(3) - (3)].sym)->u.wparam->dim_sizes[i];
			sprintf(loc->dim_refs[i],"(%s_dimsizes[%d])",tmp->name,i);
		}
		loc->datatype = (yyvsp[(3) - (3)].sym)->u.wparam->datatype;
		(yyval.ploc) = loc;
		
	}
    break;

  case 96:
#line 1569 "wrapit.y"
    {
		WParamLoc *loc = NewParamLoc((yyvsp[(1) - (4)].sym));
		WCentry *tmp;
		char buffer[BUFFSIZE];
		NclSymbol *s;

		DoTotal((yyvsp[(4) - (4)].sym));
		loc->type = TOTALPARAM;
		loc->xfarg = (yyvsp[(1) - (4)].sym);
		loc->n_dims = 1;
		loc->datatype = NCL_int;
		loc->dim_sizes[0] = 1;
		sprintf(loc->dim_refs[0],"(1)");
		sprintf(buffer,"%s_total",(yyvsp[(4) - (4)].sym)->name);
		s = _NclLookUpInScope(current->crec,buffer);
		loc->cdef = s->u.centry;
		sprintf(loc->call_string,"&%s",buffer);
		(yyval.ploc) = loc;
	}
    break;

  case 97:
#line 1589 "wrapit.y"
    {
                WParamLoc *loc = NewParamLoc((yyvsp[(1) - (4)].sym));
		char buffer[BUFFSIZE];
		NclSymbol *s;
		DoDimsizes((yyvsp[(4) - (4)].sym));

                loc->type = DIMSPARAM;
                loc->xfarg = (yyvsp[(1) - (4)].sym);
		loc->n_dims = 1;
		loc->datatype = NCL_int;
		loc->dim_sizes[0] = (yyvsp[(4) - (4)].sym)->u.wparam->n_dims;
		sprintf(loc->dim_refs[0],"(%d)",(yyvsp[(4) - (4)].sym)->u.wparam->n_dims);
		sprintf(buffer,"%s_dimsizes",(yyvsp[(4) - (4)].sym)->name);
		s = _NclLookUpInScope(current->crec,buffer);
                loc->cdef = s->u.centry;
		sprintf(loc->call_string,"%s",buffer);
		(yyval.ploc) = loc;

	}
    break;

  case 98:
#line 1609 "wrapit.y"
    {
                WParamLoc *loc = NewParamLoc((yyvsp[(1) - (7)].sym));
		char buffer[BUFFSIZE];
		NclSymbol *s;

		DoDimsizes((yyvsp[(4) - (7)].sym));

                loc->type = DIMINDEXPARAM;
                loc->xfarg = (yyvsp[(1) - (7)].sym);
		loc->n_dims = 1;
		loc->dim_sizes[0] = 1;
		sprintf(loc->dim_refs[0],"(1)");
		sprintf(buffer,"%s_dimsizes",(yyvsp[(4) - (7)].sym)->name);
		s = _NclLookUpInScope(current->crec,buffer);
                loc->cdef = s->u.centry;
		sprintf(loc->call_string,"&(%s[%d])",buffer,(yyvsp[(6) - (7)].integer));
		(yyval.ploc) = loc;
	}
    break;

  case 99:
#line 1644 "wrapit.y"
    {
		(yyval.dsp) = (WDSpecList*)malloc(sizeof(WDSpecList));
		(yyval.dsp)->type = INTDIMSIZES;
		(yyval.dsp)->u.val = (yyvsp[(1) - (1)].integer);
		(yyval.dsp)->next = NULL;
	}
    break;

  case 100:
#line 1651 "wrapit.y"
    {
		(yyval.dsp) = (WDSpecList*)malloc(sizeof(WDSpecList));
		(yyval.dsp)->type = PARAMVALDIMSIZES;
		(yyval.dsp)->u.pval.wsym = (yyvsp[(1) - (1)].sym);
		(yyval.dsp)->next = NULL;
	}
    break;

  case 101:
#line 1658 "wrapit.y"
    {
		char buffer[BUFFSIZE];
		WCentry *tmp;
		(yyval.dsp) = (WDSpecList*)malloc(sizeof(WDSpecList));
		(yyval.dsp)->type = TOTALPARAMDIMSIZES;
		(yyval.dsp)->u.tpval.wsym = (yyvsp[(2) - (2)].sym);
		(yyval.dsp)->next = NULL;
		DoTotal((yyvsp[(2) - (2)].sym));
	}
    break;

  case 102:
#line 1668 "wrapit.y"
    {
		char buffer[BUFFSIZE];
		(yyval.dsp) = (WDSpecList*)malloc(sizeof(WDSpecList));
		(yyval.dsp)->type = DIMSPARAMDIMSIZES;
		(yyval.dsp)->u.dpval.wsym = (yyvsp[(2) - (2)].sym);
		(yyval.dsp)->next = NULL;
		DoDimsizes((yyvsp[(2) - (2)].sym));
	}
    break;

  case 103:
#line 1677 "wrapit.y"
    {
		char buffer[BUFFSIZE];
		(yyval.dsp) = (WDSpecList*)malloc(sizeof(WDSpecList));
		(yyval.dsp)->type = DIMINDEXPARAMDIMSIZES;
		(yyval.dsp)->u.dipval.wsym = (yyvsp[(2) - (5)].sym);
		(yyval.dsp)->u.dipval.index= (yyvsp[(4) - (5)].integer);
		(yyval.dsp)->next = NULL;
		DoDimsizes((yyvsp[(2) - (5)].sym));
	}
    break;

  case 104:
#line 1687 "wrapit.y"
    {
		char buffer[BUFFSIZE];
		(yyval.dsp) = (WDSpecList*)malloc(sizeof(WDSpecList));
		(yyval.dsp)->type = PARAMVALDIMSIZES;
		(yyval.dsp)->u.pval.wsym = (yyvsp[(3) - (3)].sym);
		(yyval.dsp)->next = (yyvsp[(1) - (3)].dsp);
	}
    break;

  case 105:
#line 1695 "wrapit.y"
    {
		char buffer[BUFFSIZE];
		WCentry *tmp;
		(yyval.dsp) = (WDSpecList*)malloc(sizeof(WDSpecList));
		(yyval.dsp)->type = TOTALPARAMDIMSIZES;
		(yyval.dsp)->u.pval.wsym = (yyvsp[(4) - (4)].sym);
		(yyval.dsp)->next = (yyvsp[(1) - (4)].dsp);
		DoTotal((yyvsp[(4) - (4)].sym));
	}
    break;

  case 106:
#line 1705 "wrapit.y"
    {
		char buffer[BUFFSIZE];
		(yyval.dsp) = (WDSpecList*)malloc(sizeof(WDSpecList));
		(yyval.dsp)->type = DIMSPARAMDIMSIZES;
		(yyval.dsp)->u.dpval.wsym = (yyvsp[(4) - (4)].sym);
		(yyval.dsp)->next = (yyvsp[(1) - (4)].dsp);
		DoDimsizes((yyvsp[(4) - (4)].sym));
	}
    break;

  case 107:
#line 1714 "wrapit.y"
    {
		char buffer[BUFFSIZE];
		(yyval.dsp) = (WDSpecList*)malloc(sizeof(WDSpecList));
		(yyval.dsp)->type = DIMINDEXPARAMDIMSIZES;
		(yyval.dsp)->u.dipval.wsym = (yyvsp[(4) - (7)].sym);
		(yyval.dsp)->u.dipval.index= (yyvsp[(6) - (7)].integer);
		(yyval.dsp)->next = (yyvsp[(1) - (7)].dsp);
		DoDimsizes((yyvsp[(4) - (7)].sym));
	}
    break;

  case 108:
#line 1724 "wrapit.y"
    {
		char buffer[BUFFSIZE];
		(yyval.dsp) = (WDSpecList*)malloc(sizeof(WDSpecList));
		(yyval.dsp)->type = INTDIMSIZES;
		(yyval.dsp)->u.val = (yyvsp[(3) - (3)].integer);
		(yyval.dsp)->next = (yyvsp[(1) - (3)].dsp);
	}
    break;

  case 109:
#line 1760 "wrapit.y"
    {
		(yyval.dsp) = (WDSpecList*)malloc(sizeof(WDSpecList));
		(yyval.dsp)->type = PARAMVALDIMSIZES;
		(yyval.dsp)->u.pval.wsym = (yyvsp[(1) - (1)].sym);
		(yyval.dsp)->next = NULL;
	}
    break;

  case 110:
#line 1767 "wrapit.y"
    {
		char buffer[BUFFSIZE];
		WCentry *tmp;
		(yyval.dsp) = (WDSpecList*)malloc(sizeof(WDSpecList));
		(yyval.dsp)->type = TOTALPARAMDIMSIZES;
		(yyval.dsp)->u.tpval.wsym = (yyvsp[(2) - (2)].sym);
		(yyval.dsp)->next = NULL;
		DoTotal((yyvsp[(2) - (2)].sym));
	}
    break;

  case 111:
#line 1777 "wrapit.y"
    {
		char buffer[BUFFSIZE];
		(yyval.dsp) = (WDSpecList*)malloc(sizeof(WDSpecList));
		(yyval.dsp)->type = DIMSPARAMDIMSIZES;
		(yyval.dsp)->u.dpval.wsym = (yyvsp[(2) - (2)].sym);
		(yyval.dsp)->next = NULL;
		DoDimsizes((yyvsp[(2) - (2)].sym));
	}
    break;

  case 112:
#line 1786 "wrapit.y"
    {
		char buffer[BUFFSIZE];
		(yyval.dsp) = (WDSpecList*)malloc(sizeof(WDSpecList));
		(yyval.dsp)->type = DIMINDEXPARAMDIMSIZES;
		(yyval.dsp)->u.dipval.wsym = (yyvsp[(2) - (5)].sym);
		(yyval.dsp)->u.dipval.index= (yyvsp[(4) - (5)].integer);
		(yyval.dsp)->next = NULL;
		DoDimsizes((yyvsp[(2) - (5)].sym));
	}
    break;

  case 113:
#line 1796 "wrapit.y"
    {
	}
    break;

  case 114:
#line 1799 "wrapit.y"
    {
        }
    break;

  case 115:
#line 1802 "wrapit.y"
    {
        }
    break;

  case 116:
#line 1805 "wrapit.y"
    {
        }
    break;

  case 117:
#line 1825 "wrapit.y"
    {
		(yyval.mod) = NULL;
	}
    break;

  case 118:
#line 1829 "wrapit.y"
    {
		(yyval.mod)= (yyvsp[(1) - (1)].mod);
	}
    break;

  case 119:
#line 1835 "wrapit.y"
    {
		(yyvsp[(2) - (2)].mod)->next = NULL;
		(yyval.mod) = (yyvsp[(2) - (2)].mod);
	}
    break;

  case 120:
#line 1840 "wrapit.y"
    {
		(yyvsp[(3) - (3)].mod)->next = (yyvsp[(1) - (3)].mod);
		(yyval.mod) = (yyvsp[(3) - (3)].mod);
	}
    break;

  case 121:
#line 1847 "wrapit.y"
    {
		(yyval.mod) = NULL;
	}
    break;

  case 122:
#line 1851 "wrapit.y"
    {
		int i;
		char buffer[BUFFSIZE];
		NclSymbol *s;

		(yyval.mod) = (WModList*)malloc(sizeof(WModList));
		(yyval.mod)->type = DIMSIZESOF;
		switch((yyvsp[(3) - (3)].dsp)->type) {
		case PARAMVALDIMSIZES:
			(yyval.mod)->u.dims.sym = (yyvsp[(3) - (3)].dsp)->u.pval.wsym;
			(yyval.mod)->u.dims.index = -1;
			(yyval.mod)->u.dims.kind = 0;
			if((yyvsp[(3) - (3)].dsp)->u.pval.wsym->u.wparam->n_dims == 1) {
				if((yyvsp[(3) - (3)].dsp)->u.pval.wsym->u.wparam->dim_sizes[0] != -1) {
					theparam->typeofdim = 0;
					theparam->n_dims = (yyvsp[(3) - (3)].dsp)->u.pval.wsym->u.wparam->dim_sizes[0];
					for(i = 0; i < (yyvsp[(3) - (3)].dsp)->u.pval.wsym->u.wparam->dim_sizes[0]; i++) {
						sprintf(theparam->dim_refs[i],"(%s[%d])",(yyvsp[(3) - (3)].dsp)->u.pval.wsym->name);
						theparam->dim_sizes[i] = -1;
					}
				} else {
					theparam->n_dims = 1;
					theparam->dim_sizes[0] = -1;
					theparam->typeofdim = 1;
					sprintf(buffer,"%s",(yyvsp[(3) - (3)].dsp)->u.pval.wsym->name);
					s = _NclLookUpInScope(current->crec,buffer);
					theparam->altdimsref = s->u.centry; 

					sprintf(theparam->altndimref,"%s_dimsizes[0]",(yyvsp[(3) - (3)].dsp)->u.pval.wsym->name);
				}
			} else if((yyvsp[(3) - (3)].dsp)->u.pval.wsym->u.wparam->n_dims == -1) {
				theparam->n_dims = 1;
				theparam->dim_sizes[0] = -1;
				theparam->typeofdim = 1;
                                sprintf(buffer,"%s",(yyvsp[(3) - (3)].dsp)->u.pval.wsym->name);
                                s = _NclLookUpInScope(current->crec,buffer);
                                theparam->altdimsref = s->u.centry; 
                                sprintf(theparam->altndimref,"%s_dimsizes[0]",(yyvsp[(3) - (3)].dsp)->u.pval.wsym->name); 
			} else {
				fprintf(stderr,"Can not use a multidimensional variable (%s) to specify dimension sizes",(yyvsp[(3) - (3)].dsp)->u.pval.wsym->name);
			}
		break;
		case DIMSPARAMDIMSIZES:
			(yyval.mod)->u.dims.sym = (yyvsp[(3) - (3)].dsp)->u.dpval.wsym;
			(yyval.mod)->u.dims.index = -1;
			(yyval.mod)->u.dims.kind = 1;
			theparam->typeofdim = 0;
			if((yyvsp[(3) - (3)].dsp)->u.dpval.wsym->u.wparam->n_dims != -1) {
				for(i = 0; i <  (yyvsp[(3) - (3)].dsp)->u.dpval.wsym->u.wparam->n_dims; i++) {
					if((yyvsp[(3) - (3)].dsp)->u.dpval.wsym->u.wparam->dim_sizes[i] == -1) {
						sprintf(theparam->dim_refs[i],"(%s_dimsizes[%d])",(yyvsp[(3) - (3)].dsp)->u.dpval.wsym->name);
						theparam->dim_sizes[i] = -1;
					} else {
						sprintf(theparam->dim_refs[i],"(%d)",(yyvsp[(3) - (3)].dsp)->u.dpval.wsym->u.wparam->dim_sizes[i]);
						theparam->dim_sizes[i] = (yyvsp[(3) - (3)].dsp)->u.dpval.wsym->u.wparam->dim_sizes[i];
					}
				}
			} else {
				theparam->n_dims = -1;
				theparam->dim_sizes[0] = -1;
				theparam->typeofdim = 1;
                                sprintf(buffer,"%s_dimsizes",(yyvsp[(3) - (3)].dsp)->u.dpval.wsym->name);
                                s = _NclLookUpInScope(current->crec,buffer);
                                theparam->altdimsref = s->u.centry;
                                sprintf(theparam->altndimref,"%s_ndims",(yyvsp[(3) - (3)].dsp)->u.dpval.wsym->name);

			}
		break;
		case TOTALPARAMDIMSIZES:
			(yyval.mod)->u.dims.sym = (yyvsp[(3) - (3)].dsp)->u.tpval.wsym;
			(yyval.mod)->u.dims.index = -1;
			(yyval.mod)->u.dims.kind = 2;
			theparam->dim_sizes[0] = -1;
			theparam->typeofdim = 0;
			theparam->n_dims = 1;
			sprintf(theparam->dim_refs[0],"(%s_total)",(yyvsp[(3) - (3)].dsp)->u.tpval.wsym->name);
		break;
		case DIMINDEXPARAMDIMSIZES:
			(yyval.mod)->u.dims.sym = (yyvsp[(3) - (3)].dsp)->u.dipval.wsym;
			(yyval.mod)->u.dims.index = (yyvsp[(3) - (3)].dsp)->u.dipval.index;
			(yyval.mod)->u.dims.kind = 3;
			theparam->typeofdim = 0;
			theparam->dim_sizes[0] = (yyvsp[(3) - (3)].dsp)->u.dpval.wsym->u.wparam->dim_sizes[ (yyvsp[(3) - (3)].dsp)->u.dipval.index];
			theparam->n_dims = 1;
			sprintf(theparam->dim_refs[0],"(%s_dimsizes[%d])",(yyvsp[(3) - (3)].dsp)->u.dipval.wsym->name,(yyvsp[(3) - (3)].dsp)->u.dipval.index);
		break;
		} 

		
	}
    break;

  case 123:
#line 1942 "wrapit.y"
    {
		(yyval.mod) = NULL;
	}
    break;

  case 124:
#line 1946 "wrapit.y"
    {
		(yyval.mod) = NULL;
	}
    break;

  case 125:
#line 1950 "wrapit.y"
    {
		(yyval.mod) = NULL;
	}
    break;

  case 126:
#line 1954 "wrapit.y"
    {
		(yyval.mod) = (WModList*)malloc(sizeof(WModList));
		(yyval.mod)->type = TYPEOF;
		(yyval.mod)->u.type.datatype = (yyvsp[(3) - (3)].integer);
	}
    break;

  case 127:
#line 1960 "wrapit.y"
    {
		(yyval.mod) = (WModList*)malloc(sizeof(WModList));
		(yyval.mod)->type = TYPEOF;
		switch((yyvsp[(3) - (3)].dsp)->type) {
		case PARAMVALDIMSIZES:
			(yyval.mod)->u.type.datatype = (yyvsp[(3) - (3)].dsp)->u.pval.wsym->u.wparam->datatype;
			theparam->datatype = (yyvsp[(3) - (3)].dsp)->u.pval.wsym->u.wparam->datatype;
		break;
		case DIMSPARAMDIMSIZES:
			(yyval.mod)->u.type.datatype = NCL_int;
			theparam->datatype = NCL_int;
		break;
		case TOTALPARAMDIMSIZES:
			(yyval.mod)->u.type.datatype = NCL_int;
			theparam->datatype = NCL_int;
		break;
		case DIMINDEXPARAMDIMSIZES:
			(yyval.mod)->u.type.datatype = NCL_int;
			theparam->datatype = NCL_int;
		break;
		} 
	}
    break;

  case 128:
#line 1983 "wrapit.y"
    {
		(yyval.mod) = NULL;
	}
    break;

  case 129:
#line 1987 "wrapit.y"
    {
		(yyval.mod) = NULL;
	}
    break;

  case 130:
#line 1991 "wrapit.y"
    {
		(yyval.mod) = NULL;
	}
    break;

  case 131:
#line 1995 "wrapit.y"
    {
		(yyval.mod) = NULL;
	}
    break;


/* Line 1267 of yacc.c.  */
#line 3893 "y.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}


#line 2006 "wrapit.y"

void yyerror
#if __STDC__
(const char *s)
#else
(s)
	char *s;
#endif
{
	char error_buffer[1024];
	int i;

	
	fprintf(stderr,"A syntax error occurred while parsing: %s\n",yytext);
	exit(0);
}

