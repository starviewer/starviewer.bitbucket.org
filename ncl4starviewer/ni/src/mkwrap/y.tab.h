/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton interface for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     EOLN = 258,
     ENDOFIF = 259,
     INT = 260,
     BYTE1 = 261,
     INTEGER2 = 262,
     INTEGER4 = 263,
     INTEGER8 = 264,
     REAL4 = 265,
     REAL8 = 266,
     DIMENSION = 267,
     DATA = 268,
     SAVE = 269,
     COMMON = 270,
     IMPLICIT = 271,
     WPARAM = 272,
     FNAME = 273,
     UNDEF = 274,
     SUBROUTINE = 275,
     FUNCTION = 276,
     NEW = 277,
     IN = 278,
     OUT = 279,
     INOUT = 280,
     TYPE = 281,
     NOMISSING = 282,
     DIMSIZES = 283,
     PROCEDURE = 284,
     MISSING = 285,
     RETURN = 286,
     REAL = 287,
     PRECISION = 288,
     INTEGER = 289,
     FLOAT = 290,
     LONG = 291,
     DOUBLE = 292,
     BYTE = 293,
     CHARACTER = 294,
     STRNG = 295,
     SHORT = 296,
     LOGICAL = 297,
     FARG = 298,
     FUNC = 299,
     SUBR = 300,
     PROC = 301,
     XFARG = 302,
     CVAR = 303
   };
#endif
/* Tokens.  */
#define EOLN 258
#define ENDOFIF 259
#define INT 260
#define BYTE1 261
#define INTEGER2 262
#define INTEGER4 263
#define INTEGER8 264
#define REAL4 265
#define REAL8 266
#define DIMENSION 267
#define DATA 268
#define SAVE 269
#define COMMON 270
#define IMPLICIT 271
#define WPARAM 272
#define FNAME 273
#define UNDEF 274
#define SUBROUTINE 275
#define FUNCTION 276
#define NEW 277
#define IN 278
#define OUT 279
#define INOUT 280
#define TYPE 281
#define NOMISSING 282
#define DIMSIZES 283
#define PROCEDURE 284
#define MISSING 285
#define RETURN 286
#define REAL 287
#define PRECISION 288
#define INTEGER 289
#define FLOAT 290
#define LONG 291
#define DOUBLE 292
#define BYTE 293
#define CHARACTER 294
#define STRNG 295
#define SHORT 296
#define LOGICAL 297
#define FARG 298
#define FUNC 299
#define SUBR 300
#define PROC 301
#define XFARG 302
#define CVAR 303




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 115 "wrapit.y"
{
	int integer;
	char *str;
	struct _NclSymbol *sym;
	struct _DimVal *dim;
	struct _FTypeVal *ftyp;
	void *src_node;
	WParamLoc  *ploc;
	WDSpecList *dsp;
	WModList *mod;
	struct int_list *ilist;
	struct wrap_src_node_list *list;
}
/* Line 1529 of yacc.c.  */
#line 159 "y.tab.h"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;

