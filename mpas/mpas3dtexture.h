#ifndef MPAS3DTEXTURE_H
#define MPAS3DTEXTURE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include <QtOpenGL>

#include "point3d.h"
#include "geompas.h"
#include "texture1d.h"

using namespace std;

#include <math.h>

class MPAS3dTexture
{
    public:
        MPAS3dTexture();
       ~MPAS3dTexture();

        void set_var(double *var) { _var = var; };

        void reset();

        void w2r() { _texture1d->w2r(); };

        void display(int tl);

        void set_geompas(GeoMPAS *geompas);
        void set_leveler(Leveler *leveler) { _leveler = leveler; };

    private:
        GeoMPAS   *_geompas;
        Leveler   *_leveler;
        Texture1d *_texture1d;

        void _compile_display_list(int tl);

        int   _display_layers;
        int  *_display_list;

        double  *_var;

        float *_desired_level;
        double _alfa;
        double _beta;

        void _draw(int vertice[], int nz, int vertexDegree);
};
#endif

