#$ID

! include( ../common.pri ) {
    error( Couldn't find the ../common.pri file! )
}

#List of header files

 HEADERS =	picking.h

#List of source files

 SOURCES =	picking.cpp

#List of extra needed incs:

#INCLUDEPATH	+=	../translator

#List of extra needed libs:

#QMAKE_LIBDIR	+=	../translator

#LIBS		+=	-ltranslator

